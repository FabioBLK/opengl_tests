#pragma once
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera
{
public:
	Camera(GLFWwindow* p_window);
	void moveForward(float p_deltaTime);
	void moveBackwards(float p_deltaTime);
	void moveLeft(float p_deltaTime);
	void moveRight(float p_deltaTime);
	void mouse_callback(GLFWwindow* window, double xpos, double ypos);
	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
	glm::vec3 getDirection();
	glm::vec3 getCameraFront();
	glm::vec3 getCameraPosition();
	glm::vec3 getCameraUp();
	float getFov();
	glm::mat4 calculateView();
	glm::mat4 calculateProjection();

private:
	float m_lastX = 1280 / 2;
	float m_lastY = 720 / 2;
	float m_yaw = 0.0f;
	float m_pitch = 0.0f;
	float m_fov = 45.0f;
	glm::vec3 m_cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
	glm::vec3 m_cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 m_cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	float m_speed = 2.5f;
};

