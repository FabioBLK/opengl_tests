#include "AssimpModel.h"

AssimpModel::AssimpModel(std::string path, bool p_flipTexure) {
	m_modelMatrix = glm::mat4(1.0f);
	m_position = glm::vec3(0.0f);
	m_scale = glm::vec3(1.0f);
	m_flipTexture = p_flipTexure;
	m_whiteTextureId = WhiteTexture();
	loadModel(path);
}

void AssimpModel::Draw(Shader& shader) {
	shader.use();
	for (unsigned int i = 0; i < m_meshes.size(); i++) {
		m_meshes[i].Draw(shader, m_materials_loaded[m_meshes[i].m_materialIndex], m_whiteTextureId, false, 0);
	}
}

void AssimpModel::DrawModel(Shader& shader) {
	shader.use();

	m_modelMatrix = glm::mat4(1.0f);
	m_modelMatrix = glm::translate(m_modelMatrix, m_position);
	m_modelMatrix = glm::scale(m_modelMatrix, m_scale);

	shader.setMat4Float("model", m_modelMatrix);

	for (unsigned int i = 0; i < m_meshes.size(); i++) {
		m_meshes[i].Draw(shader, m_materials_loaded[m_meshes[i].m_materialIndex], m_whiteTextureId, false, 0);
	}
}

void AssimpModel::DrawInstanced(Shader& shader, const unsigned int p_amount) {
	for (unsigned int i = 0; i < m_meshes.size(); i++) {
		m_meshes[i].Draw(shader, m_materials_loaded[m_meshes[i].m_materialIndex], m_whiteTextureId, true, p_amount);
	}
}

void AssimpModel::loadModel(std::string path) {
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs |
		aiProcess_FindInvalidData | aiProcess_GenSmoothNormals | aiProcess_OptimizeMeshes);

	if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
		std::string errorStr = importer.GetErrorString();
		std::string message = "ERROR::ASSIMP::" + errorStr;
		FLogs::shared_instance().Log(message);
		return;
	}

	m_directory = path.substr(0, path.find_last_of('/'));
	processNode(scene->mRootNode, scene);
}

void AssimpModel::processNode(aiNode* node, const aiScene* scene) {
	// process all the node's meshes (if any)
	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		m_meshes.push_back(processMesh(mesh, scene));
	}

	// then do the same for each of its children
	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		processNode(node->mChildren[i], scene);
	}
}

Mesh AssimpModel::processMesh(aiMesh* mesh, const aiScene* scene) {
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
		Vertex vertex;

		// process vertex positions, normals and texture coordinates
		glm::vec3 vector;
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;

		vertex.Position = vector;

		if (mesh->HasNormals()) {
			// process vertex normals
			glm::vec3 normals;
			normals.x = mesh->mNormals[i].x;
			normals.y = mesh->mNormals[i].y;
			normals.z = mesh->mNormals[i].z;

			vertex.Normal = normals;
		}
		else {
			FLogs::shared_instance().Log("ERROR::AssimpModel::Normals not found in model");
		}

		// process vertex Texture coordinates
		if (mesh->mTextureCoords[0]) { // does the mesh contain texture coordinates?
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;

			vertex.TexCoords = vec;
		}
		else {
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
		}

		vertices.push_back(vertex);
	}

	// process indices
	for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];
		for (unsigned int j = 0; j < face.mNumIndices; j++) {
			indices.push_back(face.mIndices[j]);
		}
	}

	unsigned int materialIndex = 0;
	// process material
	if (mesh->mMaterialIndex >= 0) {
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		std::vector<Texture> materialTextures;

		std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, MeshTextureType::TEXTURE_DIFFUSE);
		materialTextures.insert(materialTextures.end(), diffuseMaps.begin(), diffuseMaps.end());

		std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, MeshTextureType::TEXTURE_SPECULAR);
		std::string specularMessage = "INFO::AssimpModel::spec count " + std::to_string(specularMaps.size());
		FLogs::shared_instance().Log(specularMessage);
		materialTextures.insert(materialTextures.end(), specularMaps.begin(), specularMaps.end());

		materialIndex = mesh->mMaterialIndex;
		std::string textureMessage = "INFO::AssimpModel::Material Index " + std::to_string(materialIndex) + " has " + std::to_string(materialTextures.size()) + " textures";
		FLogs::shared_instance().Log(textureMessage);
		Material mat;
		mat.id = materialIndex;
		mat.textures = materialTextures;
		aiColor3D difColor(1.0f, 1.0f, 1.0f);
		aiColor3D specColor(1.0f, 1.0f, 1.0f);
		float shineness = 0.0f;
		if (AI_SUCCESS == material->Get(AI_MATKEY_COLOR_DIFFUSE, difColor)) {
			if (material->GetTextureCount(aiTextureType_DIFFUSE) > 0 && difColor.IsBlack()) {
				mat.diffuseColor = glm::vec3(1.0f, 1.0f, 1.0f);
			}
			else {
				mat.diffuseColor = glm::vec3(difColor.r, difColor.g, difColor.b);
			}
		}
		if (AI_SUCCESS == material->Get(AI_MATKEY_COLOR_SPECULAR, specColor)) {
			mat.specularColor = glm::vec3(specColor.r, specColor.g, specColor.b);
		}
		if (AI_SUCCESS == material->Get(AI_MATKEY_SHININESS, shineness)) {
			mat.shineness = shineness;
		}

		if (m_materials_loaded.find(materialIndex) == m_materials_loaded.end()) {
			m_materials_loaded.emplace(materialIndex, mat);
		}
	}

	return Mesh(vertices, indices, materialIndex);
}

std::vector<Texture> AssimpModel::loadMaterialTextures(aiMaterial* mat, aiTextureType type, MeshTextureType meshType) {
	std::vector<Texture> textures;
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++) {
		aiString str;
		mat->GetTexture(type, i, &str);

		bool skip = false;
		for (unsigned int j = 0; j < textures_loaded.size(); j++) {
			if (std::strcmp(str.C_Str(), textures_loaded[j].path.data()) == 0) {
				textures.push_back(textures_loaded[j]);
				skip = true;
				break;
			}
		}

		if (!skip) {
			Texture texture;
			texture.id = TextureFromFile(str.C_Str(), m_directory);
			texture.type = meshType;
			texture.path = str.C_Str();

			textures.push_back(texture);
			textures_loaded.push_back(texture);

			if (meshType == MeshTextureType::TEXTURE_SPECULAR) {
				std::string message = "INFO::AssimpModel::" + texture.path + " is specular";
				FLogs::shared_instance().Log(message);
			}
		}
	}
	return textures;
}

unsigned int AssimpModel::TextureFromFile(std::string path, std::string directory) {
	std::string filePath = directory + "/" + path;
	std::string loadingTexureMessage = "loading texture at " + filePath;
	FLogs::shared_instance().Log(loadingTexureMessage);
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load and generate the texture
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(m_flipTexture);
	unsigned char* data = stbi_load(filePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 4) {
			format = GL_RGBA;
		}
		else {
			format = GL_RGB;
		}
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Required on Alpha Romeo Textures
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		std::string successMessage = "INFO::AssimpModel::SUCCESS to load texture " + filePath;
		FLogs::shared_instance().Log(successMessage);
	}
	else {
		FLogs::shared_instance().Log("ERROR::AssimpModel::Failed to load texture");
	}
	stbi_image_free(data);

	return texture;
}

unsigned int AssimpModel::WhiteTexture() {
	FLogs::shared_instance().Log("INFO::AssimpModel::Generating white texture");
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load and generate the texture
	int width = 1;
	int height = 1;

	unsigned char* data = new unsigned char[width * height * sizeof(unsigned char)];
	for (int i = 0; i < (int)(width * height * sizeof(unsigned char)); i++)
	{
		data[i] = 255;
	}

	if (data) {
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Required on Alpha Romeo Textures
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		FLogs::shared_instance().Log("INFO::AssimpModel::SUCCESS to load texture");
	}
	else {
		FLogs::shared_instance().Log("ERROR::AssimpModel::Failed to load texture");
	}
	stbi_image_free(data);

	return texture;
}

void AssimpModel::SetInstances(const unsigned int p_amount, glm::mat4* p_matrices) {
	unsigned int buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, p_amount * sizeof(glm::mat4), &p_matrices[0], GL_STATIC_DRAW);

	// set transformation matrices as an instance vertex attribute (with divisor 1)
	// note: we're cheating a little by taking the, now publicly declared, VAO of the model's mesh(es) and adding new vertexAttribPointers
	// normally you'd want to do this in a more organized fashion, but for learning purposes this will do.
	for (unsigned int i = 0; i < m_meshes.size(); i++) {
		unsigned int VAO = m_meshes[i].GetVAO();
		glBindVertexArray(VAO);
		// set attribute pointers for matrix (4 times vec4)
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);

		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));

		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));

		glEnableVertexAttribArray(6);
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

		glVertexAttribDivisor(3, 1);
		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		glVertexAttribDivisor(6, 1);

		glBindVertexArray(0);
	}
}

void AssimpModel::SetPosition(const glm::vec3* p_position) {
	m_position = *p_position;
}

void AssimpModel::SetScale(const glm::vec3* p_scale) {
	m_scale = *p_scale;
}
