#version 330 core

out vec4 FragColor2;

in vec2 TexCoord;

uniform sampler2D frontTexture;
uniform sampler2D backTexture;

void main()
{
	if (gl_FrontFacing) {
		FragColor2 = texture(frontTexture, TexCoord);
	}
	else {
		FragColor2 = texture(backTexture, TexCoord);
	}
};