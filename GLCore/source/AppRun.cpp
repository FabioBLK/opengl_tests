#include "AppRun.h"

AppRun::AppRun() {

}

void AppRun::AsteroidBeltInstance() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();
	videoGL.enableBlend();
	videoGL.enableCulling();

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();

	FColor dimWhite = { 1.0f * 0.2f, 1.0f * 0.2f, 0.0f * 0.2f, 1.0f * 0.2f };

	FTime time = FTime();

	Camera camera = Camera(videoGL.getWindow());
	Shader planetShader = Shader("shader/mesh_default.vert", "shader/mesh_default.frag");
	Shader asteroidShader = Shader("shader/mesh_default_instance.vert", "shader/mesh_default.frag");
	Shader skyboxShader = Shader("shader/cubemap.vert", "shader/cubemap.frag");

	std::vector<std::string> skyboxTexturePath = {
		"assets/space-skybox/right.jpg",
		"assets/space-skybox/left.jpg",
		"assets/space-skybox/top.jpg",
		"assets/space-skybox/bottom.jpg",
		"assets/space-skybox/front.jpg",
		"assets/space-skybox/back.jpg"
	};
	unsigned int skyboxTexure = videoGL.getCubeMapTexture(skyboxTexturePath, GL_RGB, false);

	planetShader.use();
	planetShader.setVec3Float("dirLight.direction", 3.0f, 1.0f, 0.0f);
	planetShader.setVec3Float("dirLight.ambient", dimWhite.r, dimWhite.g, dimWhite.b);
	planetShader.setVec3Float("dirLight.diffuse", white.r, white.g, white.b);
	planetShader.setVec3Float("dirLight.specular", white.r, white.g, white.b);

	asteroidShader.use();
	asteroidShader.setVec3Float("dirLight.direction", 3.0f, 1.0f, 0.0f);
	asteroidShader.setVec3Float("dirLight.ambient", white.r, white.g, white.b);
	asteroidShader.setVec3Float("dirLight.diffuse", white.r, white.g, white.b);
	asteroidShader.setVec3Float("dirLight.specular", white.r, white.g, white.b);

	skyboxShader.use();
	skyboxShader.setInt("skybox", 0);

	TinyModel planet = TinyModel("assets/models/planet/planet.obj", true);
	TinyModel asteroid = TinyModel("assets/models/rock/rock.obj", true);

	unsigned int amount = 10000;
	glm::mat4* modelMatrices;
	modelMatrices = new glm::mat4[amount];
	srand(glfwGetTime());
	float radius = 75.0f;
	float offset = 25.0f;

	for (unsigned int i = 0; i < amount; i++) {
		glm::mat4 model = glm::mat4(1.0f);
		// 1. translation: displace along circle with 'radius' in range [-offset, offset]
		float angle = (float)i / (float)amount * 360.0f;
		float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float x = sin(angle) * radius + displacement;
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float y = displacement * 0.4f; // keep height of field smaller compared to width of x and z
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float z = cos(angle) * radius + displacement;
		model = glm::translate(model, glm::vec3(x, y, z));

		// 2. scale: scale between 0.05 and 0.25f
		float scale = (rand() % 20) / 100.0f + 0.05f;
		model = glm::scale(model, glm::vec3(scale));

		// 3. rotation: add random rotation around a (semi)randomly picked rotation axis vector
		float rotAngle = (rand() % 360);
		model = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8));

		// 4. now add to list of matrices
		modelMatrices[i] = model;
	}

	asteroid.SetInstances(amount, modelMatrices);
	unsigned int skyboxVAO = videoGL.prepareCubeMapCube();

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();
		glm::vec3 camViewPos = camera.getCameraPosition();

		planetShader.use();
		planetShader.setMat4Float("view", view);
		planetShader.setMat4Float("projection", projection);
		planetShader.setVec3Float("viewPos", camViewPos.x, camViewPos.y, camViewPos.z);

		asteroidShader.use();
		asteroidShader.setMat4Float("view", view);
		asteroidShader.setMat4Float("projection", projection);
		asteroidShader.setVec3Float("viewPos", camViewPos.x, camViewPos.y, camViewPos.z);

		planetShader.use();
		glm::mat4 planetMatrix = glm::mat4(1.0f);
		planetMatrix = glm::translate(planetMatrix, glm::vec3(0.0f, 0.0f, -1.0f));
		planetMatrix = glm::scale(planetMatrix, glm::vec3(4.0f, 4.0f, 4.0f));
		planetShader.setMat4Float("model", planetMatrix);

		planet.Draw(planetShader);

		asteroidShader.use();
		asteroid.DrawInstanced(asteroidShader, amount);

		// SKYBOX START
		glDepthFunc(GL_LEQUAL);
		skyboxShader.use();
		glm::mat4 fixView = glm::mat4(glm::mat3(view));
		skyboxShader.setMat4Float("view", fixView);
		skyboxShader.setMat4Float("projection", projection);
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexure);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glDepthFunc(GL_LESS);
		// SKYBOX END

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::AsteroidBelt() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();
	videoGL.enableBlend();
	videoGL.enableCulling();
	glEnable(GL_PROGRAM_POINT_SIZE);

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();

	FColor dimWhite = { 1.0f * 0.2f, 1.0f * 0.2f, 0.0f * 0.2f, 1.0f * 0.2f };

	FTime time = FTime();

	Camera camera = Camera(videoGL.getWindow());
	Shader shader01 = Shader("shader/mesh_default.vert", "shader/mesh_default.frag");

	shader01.use();
	shader01.setVec3Float("dirLight.direction", 3.0f, 1.0f, 0.0f);
	shader01.setVec3Float("dirLight.ambient", white.r, white.g, white.b);
	shader01.setVec3Float("dirLight.diffuse", white.r, white.g, white.b);
	shader01.setVec3Float("dirLight.specular", white.r, white.g, white.b);

	TinyModel planet = TinyModel("assets/models/planet/planet.obj", true);
	TinyModel asteroid = TinyModel("assets/models/rock/rock.obj", true);

	unsigned int amount = 1000;
	glm::mat4* modelMatrices;
	modelMatrices = new glm::mat4[amount];
	srand(glfwGetTime());
	float radius = 50.0f;
	float offset = 2.5f;

	for (unsigned int i = 0; i < amount; i++) {
		glm::mat4 model = glm::mat4(1.0f);
		// 1. translation: displace along circle with 'radius' in range [-offset, offset]
		float angle = (float)i / (float)amount * 360.0f;
		float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float x = sin(angle) * radius + displacement;
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float y = displacement * 0.4f; // keep height of field smaller compared to width of x and z
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float z = cos(angle) * radius + displacement;
		model = glm::translate(model, glm::vec3(x, y, z));

		// 2. scale: scale between 0.05 and 0.25f
		float scale = (rand() % 20) / 100.0f + 0.05f;
		model = glm::scale(model, glm::vec3(scale));

		// 3. rotation: add random rotation around a (semi)randomly picked rotation axis vector
		float rotAngle = (rand() % 360);
		model = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8));

		// 4. now add to list of matrices
		modelMatrices[i] = model;
	}

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();
		glm::vec3 camViewPos = camera.getCameraPosition();

		shader01.use();
		shader01.setMat4Float("view", view);
		shader01.setMat4Float("projection", projection);
		shader01.setVec3Float("viewPos", camViewPos.x, camViewPos.y, camViewPos.z);

		glm::mat4 planetMatrix = glm::mat4(1.0f);
		planetMatrix = glm::translate(planetMatrix, glm::vec3(0.0f, 0.0f, -1.0f));
		planetMatrix = glm::scale(planetMatrix, glm::vec3(4.0f, 4.0f, 4.0f));
		shader01.setMat4Float("model", planetMatrix);

		planet.Draw(shader01);

		for (int i = 0; i < amount; i++) {
			shader01.setMat4Float("model", modelMatrices[i]);
			asteroid.Draw(shader01);
		}

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::InstancingTest() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();
	videoGL.enableBlend();
	//videoGL.enableCulling();
	glEnable(GL_PROGRAM_POINT_SIZE);

	Camera camera = Camera(videoGL.getWindow());
	Shader shader01 = Shader("shader/vec3_color_texture_offset_layout.vert", "shader/in_vec3_color.frag");

	FTime time = FTime();

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();

	unsigned int pointsVAO = videoGL.prepareColorRectangleOffset();

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		shader01.use();
		shader01.setMat4Float("view", view);
		shader01.setMat4Float("projection", projection);

		glm::mat4 pointTinyModel = glm::mat4(1.0f);
		pointTinyModel = glm::translate(pointTinyModel, glm::vec3(0.0f, 0.0f, -1.0f));
		shader01.setMat4Float("model", pointTinyModel);

		glBindVertexArray(pointsVAO);
		glDrawArraysInstanced(GL_TRIANGLES, 0, 6, 100);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::NormalDebugModels() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();

	Camera camera = Camera(videoGL.getWindow());

	Shader shaderMaterial = Shader("shader/mesh_default.vert", "shader/mesh_default.frag");
	Shader shaderColor01 = Shader("shader/vec3_texture.vert", "shader/unif_vec4_tex.frag");
	Shader skyboxShader = Shader("shader/cubemap.vert", "shader/cubemap.frag");
	Shader debugShader = Shader("shader/debug_normal.vert", "shader/unif_vec4_color_static.frag", "shader/debug_normal.geom");

	glm::vec3 yellowColor = glm::vec3(1.0f, 1.0f, 0.92f);
	glm::vec3 whiteColor = glm::vec3(1.0f, 1.0f, 1.0f);
	glm::vec3 redColor = glm::vec3(1.0f, 0.5f, 0.5f);

	std::vector<std::string> skyboxTexturePath = {
	"assets/skybox/right.jpg",
	"assets/skybox/left.jpg",
	"assets/skybox/top.jpg",
	"assets/skybox/bottom.jpg",
	"assets/skybox/front.jpg",
	"assets/skybox/back.jpg"
	};
	unsigned int skyboxTexure = videoGL.getCubeMapTexture(skyboxTexturePath, GL_RGB, false);

	shaderMaterial.use();
	shaderMaterial.setVec3Float("dirLight.diffuse", yellowColor.x, yellowColor.y, yellowColor.z);
	shaderMaterial.setVec3Float("dirLight.ambient", yellowColor.x * 0.2f, yellowColor.y * 0.2f, yellowColor.z * 0.2f);
	shaderMaterial.setVec3Float("dirLight.specular", yellowColor.x, yellowColor.y, yellowColor.z);
	shaderMaterial.setVec3Float("dirLight.direction", -0.2f, -1.0f, -0.3f);
	shaderMaterial.setInt("skybox", 11);

	skyboxShader.use();
	skyboxShader.setInt("skybox", 0);

	float deltaTime = 0.0f;
	float lastFrame = 0.0f;
	float angle = 20.0f;
	float lightAngle = 20.0f;

	int pointLightCount = 4;
	glm::vec3 pointLights[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
		glm::vec3(2.3f, -3.3f, -4.0f),
		glm::vec3(-4.0f,  2.0f, -12.0f),
		glm::vec3(0.0f,  0.0f, -3.0f)
	};

	glm::vec3 pointLightColors[] = {
		whiteColor,
		whiteColor,
		whiteColor,
		whiteColor
	};

	shaderColor01.use();
	shaderColor01.setVec3Float("myColor", pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
	shaderColor01.setVec3Float("myLight", pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);

	glm::vec3 backPackPos = glm::vec3(0.0f, -2.0f, -3.0f);
	//std::string backPackPath = "assets/models/car/Alfa_Romeo_Giulia_GTAm.obj";
	std::string backPackPath = "assets/models/backpack/backpack.obj";
	//std::string backPackPath = "assets/models/mustang/mustang.obj";
	//std::string backPackPath = "assets/models/sponza/sponza.obj";

	std::string sunPath = "assets/models/EggSun/egg_sun.obj";
	TinyModel backPack = TinyModel(backPackPath, true);
	TinyModel sun = TinyModel(sunPath, false);

	unsigned int skyboxVAO = videoGL.prepareCubeMapCube();

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_RIGHT)) {
			angle += 50.0f * deltaTime;
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_LEFT)) {
			angle -= 50.0f * deltaTime;
		}

		videoGL.clearFrame();

		const float radius = 5.0f;
		float lightX = sin(glfwGetTime()) * radius;
		float lightZ = cos(glfwGetTime()) * radius;

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		shaderColor01.use();
		shaderColor01.setMat4Float("view", view);
		shaderColor01.setMat4Float("projection", projection);

		glActiveTexture(GL_TEXTURE11);
		shaderMaterial.setInt("skybox", 11);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexure);

		glm::mat4 pointTinyModel = glm::mat4(1.0f);
		pointTinyModel = glm::translate(pointTinyModel, glm::vec3(lightX, pointLights[0].y, lightZ));
		pointTinyModel = glm::scale(pointTinyModel, glm::vec3(0.2f));

		shaderColor01.setVec3Float("myColor", pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
		shaderColor01.setVec3Float("myLight", pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
		shaderColor01.setMat4Float("model", pointTinyModel);

		// draw point light
		sun.Draw(shaderColor01);

		shaderMaterial.use();
		shaderMaterial.setFloat("time", glfwGetTime());
		shaderMaterial.setMat4Float("view", view);
		shaderMaterial.setVec3Float("viewPos", camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);
		shaderMaterial.setMat4Float("projection", projection);

		for (int i = 0; i < pointLightCount; i++) {
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].position", lightX, pointLights[i].y, lightZ);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].ambient", pointLightColors[i].x * 0.2f, pointLightColors[i].y * 0.2f, pointLightColors[i].z * 0.2f);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].diffuse", pointLightColors[i].x, pointLightColors[i].y, pointLightColors[i].z);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].specular", pointLightColors[i].x, pointLightColors[i].y, pointLightColors[i].z);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].constant", 1.0f);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].linear", 0.09f);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].quadratic", 0.032f);
		}

		glm::mat4 matBackPack = glm::mat4(1.0f);
		matBackPack = glm::translate(matBackPack, backPackPos);
		matBackPack = glm::scale(matBackPack, glm::vec3(1.0f, 1.0f, 1.0f));
		matBackPack = glm::rotate(matBackPack, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
		shaderMaterial.setMat4Float("model", matBackPack);

		backPack.Draw(shaderMaterial);

		debugShader.use();
		debugShader.setMat4Float("view", view);
		debugShader.setMat4Float("projection", projection);
		debugShader.setMat4Float("model", matBackPack);
		backPack.Draw(debugShader);

		glBindVertexArray(0);

		// SKYBOX START
		glDepthFunc(GL_LEQUAL);
		skyboxShader.use();
		glm::mat4 fixView = glm::mat4(glm::mat3(view));
		skyboxShader.setMat4Float("view", fixView);
		skyboxShader.setMat4Float("projection", projection);
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexure);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glDepthFunc(GL_LESS);
		// SKYBOX END

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::GeometryShaderInit() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();
	videoGL.enableBlend();
	//videoGL.enableCulling();
	glEnable(GL_PROGRAM_POINT_SIZE);

	Camera camera = Camera(videoGL.getWindow());
	Shader shader01 = Shader("shader/simple_default_color_vs_out.vert", "shader/in_vec3_color.frag", "shader/geometry_line.geom");

	FTime time = FTime();

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();

	unsigned int pointsVAO = videoGL.prepareVertexPoints();

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		shader01.use();
		shader01.setMat4Float("view", view);
		shader01.setMat4Float("projection", projection);

		glm::mat4 pointTinyModel = glm::mat4(1.0f);
		pointTinyModel = glm::translate(pointTinyModel, glm::vec3(0.0f, 0.0f, -1.0f));
		shader01.setMat4Float("model", pointTinyModel);

		glBindVertexArray(pointsVAO);
		glDrawArrays(GL_POINTS, 0, 4);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::UniformBufferTest() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();
	videoGL.enableBlend();

	Camera camera = Camera(videoGL.getWindow());

	FTime time = FTime();

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();
	FColor red = Colors::Red();
	FColor blue = Colors::Blue();

	Shader redShader = Shader("shader/glsl_ubo_matrix.vert", "shader/unif_vec4_color.frag");
	Shader greenShader = Shader("shader/glsl_ubo_matrix.vert", "shader/unif_vec4_color.frag");
	Shader blueShader = Shader("shader/glsl_ubo_matrix.vert", "shader/unif_vec4_color.frag");
	Shader yellowShader = Shader("shader/glsl_ubo_matrix.vert", "shader/unif_vec4_color.frag");

	redShader.use();
	redShader.setVec4Float("myColor", red.r, red.g, red.b, red.a);
	greenShader.use();
	greenShader.setVec4Float("myColor", green.r, green.g, green.b, green.a);
	blueShader.use();
	blueShader.setVec4Float("myColor", blue.r, blue.g, blue.b, blue.a);
	yellowShader.use();
	yellowShader.setVec4Float("myColor", yellow.r, yellow.g, yellow.b, yellow.a);

	unsigned int uboMatrices;
	glGenBuffers(1, &uboMatrices);

	glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
	glBufferData(GL_UNIFORM_BUFFER, 2 * sizeof(glm::mat4), NULL, GL_STATIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	glBindBufferRange(GL_UNIFORM_BUFFER, 0, uboMatrices, 0, 2 * sizeof(glm::mat4));

	unsigned int cubeVAO = videoGL.prepareTexturedNormalCube();

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
		glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(glm::mat4), glm::value_ptr(projection));
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
		glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
		glBindBuffer(GL_UNIFORM_BUFFER, 0);

		glBindVertexArray(cubeVAO);
		redShader.use();
		glm::mat4 model1 = glm::mat4(1.0f);
		model1 = glm::translate(model1, glm::vec3(-0.75f, 0.75f, 0.0f));
		redShader.setMat4Float("model", model1);
		glDrawArrays(GL_TRIANGLES, 0, 36);

		greenShader.use();
		glm::mat4 model2 = glm::mat4(1.0f);
		model2 = glm::translate(model2, glm::vec3(0.75f, 0.75f, 0.0f));
		greenShader.setMat4Float("model", model2);
		glDrawArrays(GL_TRIANGLES, 0, 36);

		blueShader.use();
		glm::mat4 model3 = glm::mat4(1.0f);
		model3 = glm::translate(model3, glm::vec3(-0.75f, -0.75f, 0.0f));
		blueShader.setMat4Float("model", model3);
		glDrawArrays(GL_TRIANGLES, 0, 36);

		yellowShader.use();
		glm::mat4 model4 = glm::mat4(1.0f);
		model4 = glm::translate(model4, glm::vec3(0.75f, -0.75f, 0.0f));
		yellowShader.setMat4Float("model", model4);
		glDrawArrays(GL_TRIANGLES, 0, 36);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::AdvancedGLSL() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();
	videoGL.enableBlend();
	//videoGL.enableCulling();
	glEnable(GL_PROGRAM_POINT_SIZE);

	Camera camera = Camera(videoGL.getWindow());
	Shader shaderMaterial = Shader("shader/mesh_default.vert", "shader/glsl_frag_front_facing.frag");

	unsigned int cubeTextureD = videoGL.getTexture("assets/wall.jpg", GL_RGB, true);
	unsigned int cubeTextureS = videoGL.getTexture("assets/matrix.jpg", GL_RGB, true);

	FTime time = FTime();

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();

	shaderMaterial.use();
	//shaderMaterial.setVec4Float("myColor1", green.r, green.g, green.b, green.a);
	//shaderMaterial.setVec4Float("myColor2", yellow.r, yellow.g, yellow.b, yellow.a);
	shaderMaterial.setInt("frontTexture", 0);
	shaderMaterial.setInt("frontTexture", 1);

	unsigned int cubeVAO = videoGL.prepareTexturedNormalCube();

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		shaderMaterial.use();
		shaderMaterial.setMat4Float("view", view);
		shaderMaterial.setVec3Float("viewPos", camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);
		shaderMaterial.setMat4Float("projection", projection);

		glm::mat4 pointMat = glm::mat4(1.0f);
		pointMat = glm::translate(pointMat, glm::vec3(0.0f, 0.0f, -3.0f));
		pointMat = glm::scale(pointMat, glm::vec3(2.0f, 2.0f, 2.0f));
		shaderMaterial.setMat4Float("model", pointMat);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, cubeTextureD);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, cubeTextureS);
		glBindVertexArray(cubeVAO);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(0);
		glBindVertexArray(0);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::CubeMapTest() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();
	videoGL.enableMSAA();
	videoGL.enableBlend();
	videoGL.enableCulling();

	unsigned int bufferTexture = videoGL.getNullTexture(WIDTH_RESOLUTION, HEIGHT_RESOLUTION);
	unsigned int frameBuffer = videoGL.getFrameBufferTexture(bufferTexture);
	unsigned int bufferMultiSampleTexture = videoGL.getNullMultiSampleTexture(WIDTH_RESOLUTION, HEIGHT_RESOLUTION, MSAA_SAMPLES);
	unsigned int multiSampleBuffer = videoGL.getFrameBufferMultiSampleTexture(bufferMultiSampleTexture);

	Camera camera = Camera(videoGL.getWindow());
	Shader shaderMaterial = Shader("shader/mesh_default.vert", "shader/mesh_default.frag");
	Shader grassShader = Shader("shader/mesh_default.vert", "shader/unif_vec4_tex.frag");
	Shader skyboxShader = Shader("shader/cubemap.vert", "shader/cubemap.frag");

	// Post Processing shaders
	Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex.frag"); // Original Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_inverse.frag"); // Inverse Color Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_greyscale.frag"); // Greyscale Color Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_edge.frag"); // Edge Kernel Color Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_edge_detection.frag"); // Edge Detection Kernel Color Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_blur.frag"); // Blur Kernel Color Shader

	unsigned int grassTexture = videoGL.getTexture("assets/window.png", GL_RGBA, false);

	std::vector<std::string> skyboxTexturePath = {
		"assets/skybox/right.jpg",
		"assets/skybox/left.jpg",
		"assets/skybox/top.jpg",
		"assets/skybox/bottom.jpg",
		"assets/skybox/front.jpg",
		"assets/skybox/back.jpg"
	};
	unsigned int skyboxTexure = videoGL.getCubeMapTexture(skyboxTexturePath, GL_RGB, false);

	FTime time = FTime();

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();

	skyboxShader.use();
	skyboxShader.setInt("skybox", 0);

	shaderMaterial.use();
	shaderMaterial.setVec3Float("dirLight.diffuse", white.r, white.g, white.b);
	shaderMaterial.setVec3Float("dirLight.ambient", white.r * 0.2f, white.g * 0.2f, white.b * 0.2f);
	shaderMaterial.setVec3Float("dirLight.specular", white.r, white.g, white.b);
	shaderMaterial.setVec3Float("dirLight.direction", -0.2f, -1.0f, -0.3f);
	shaderMaterial.setFloat("pointLights[0].constant", 1.0f);
	shaderMaterial.setFloat("pointLights[0].linear", 0.09f);
	shaderMaterial.setFloat("pointLights[0].quadratic", 0.032f);
	shaderMaterial.setInt("skybox", 11);

	grassShader.use();
	grassShader.setInt("ourTexture", 0);

	screenShader.use();
	screenShader.setInt("ourTexture", 0);

	int pointLightCount = 1;
	glm::vec3 pointLights[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
	};

	FColor pointLightColors[] = {
		white
	};

	glm::vec3 cube1Pos = glm::vec3(0.0f, 0.0f, -3.0f);
	glm::vec3 cube2Pos = glm::vec3(-1.0f, 0.0f, -0.0f);
	glm::vec3 floorPos = glm::vec3(0.0f, -0.35f, 0.0f);

	std::vector<glm::vec3> grassPositions;
	grassPositions.push_back(glm::vec3(1.0f, -0.6f, 4.0f));
	grassPositions.push_back(glm::vec3(0.0f, -0.6f, 4.2f));
	grassPositions.push_back(glm::vec3(-1.0f, -0.6f, 4.1f));
	grassPositions.push_back(glm::vec3(2.0f, -0.6f, 4.3f));

	std::string cubePath = "assets/models/cube/MyCube.obj";
	std::string floorPath = "assets/models/floor/MyFloor.obj";

	TinyModel cube1TinyModel = TinyModel(cubePath, false);
	TinyModel floorTinyModel = TinyModel(floorPath, false);

	unsigned int grassVAO = videoGL.prepareTexturedRectangleNormal();
	unsigned int quadVAO = videoGL.prepareTexturedRectangleBuffer();
	unsigned int skyboxVAO = videoGL.prepareCubeMapCube();

	std::map<float, glm::vec3> sorted;

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		// First Pass
		videoGL.bindFrameBuffer(multiSampleBuffer);
		videoGL.enableDepthTest();

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		grassShader.use();
		grassShader.setMat4Float("view", view);
		grassShader.setMat4Float("projection", projection);

		shaderMaterial.use();
		shaderMaterial.setMat4Float("view", view);
		shaderMaterial.setVec3Float("viewPos", camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);
		shaderMaterial.setMat4Float("projection", projection);

		glActiveTexture(GL_TEXTURE11);
		shaderMaterial.setInt("skybox", 11);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexure);

		glm::mat4 matFloor = glm::mat4(1.0f);
		matFloor = glm::translate(matFloor, floorPos);
		matFloor = glm::scale(matFloor, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matFloor);

		floorTinyModel.Draw(shaderMaterial);

		glm::mat4 matcube1 = glm::mat4(1.0f);
		matcube1 = glm::translate(matcube1, cube1Pos);
		matcube1 = glm::scale(matcube1, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matcube1);

		cube1TinyModel.Draw(shaderMaterial);

		glm::mat4 matcube2 = glm::mat4(1.0f);
		matcube2 = glm::translate(matcube2, cube2Pos);
		matcube2 = glm::scale(matcube2, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matcube2);

		cube1TinyModel.Draw(shaderMaterial);

		// SKYBOX START
		glDepthFunc(GL_LEQUAL);
		skyboxShader.use();
		glm::mat4 fixView = glm::mat4(glm::mat3(view));
		skyboxShader.setMat4Float("view", fixView);
		skyboxShader.setMat4Float("projection", projection);
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexure);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glDepthFunc(GL_LESS);
		// SKYBOX END

		grassShader.use();
		glBindVertexArray(grassVAO);
		glBindTexture(GL_TEXTURE_2D, grassTexture);

		videoGL.disableCulling();
		sorted.clear();
		for (int i = 0; i < grassPositions.size(); i++) {
			float distance = glm::distance(camera.getCameraPosition(), grassPositions[i]);
			sorted[distance] = grassPositions[i];
		}

		for (std::map<float, glm::vec3>::reverse_iterator it = sorted.rbegin(); it != sorted.rend(); it++) {
			glm::mat4 grassMat = glm::mat4(1.0f);
			grassMat = glm::translate(grassMat, it->second);
			grassMat = glm::scale(grassMat, glm::vec3(1.0f, 1.0f, 1.0f));
			grassShader.setMat4Float("model", grassMat);
			glDrawArrays(GL_TRIANGLES, 0, 8);
		}
		videoGL.enableCulling();
		glBindVertexArray(0);

		// Swap to Intermediate FrameBuffer
		videoGL.swapFrameBuffer(multiSampleBuffer, frameBuffer);

		// Clear everything and draw frambuffer to quad texture
		unsigned int emptyFrameBuffer = 0;
		videoGL.bindFrameBuffer(emptyFrameBuffer);
		videoGL.disableDepthTest();

		videoGL.clearFrame();

		screenShader.use();
		glBindVertexArray(quadVAO);
		glBindTexture(GL_TEXTURE_2D, bufferTexture);

		// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glBindVertexArray(0);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::BlendTests() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();
	videoGL.enableBlend();
	videoGL.enableCulling();

	unsigned int bufferTexture = videoGL.getNullTexture(WIDTH_RESOLUTION, HEIGHT_RESOLUTION);
	unsigned int frameBuffer = videoGL.getFrameBufferTexture(bufferTexture);

	Camera camera = Camera(videoGL.getWindow());
	Shader shaderMaterial = Shader("shader/mesh_default.vert", "shader/mesh_default.frag");
	Shader grassShader = Shader("shader/mesh_default.vert", "shader/unif_vec4_tex.frag");

	// Post Processing shaders
	Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex.frag"); // Original Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_inverse.frag"); // Inverse Color Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_greyscale.frag"); // Greyscale Color Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_edge.frag"); // Edge Kernel Color Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_edge_detection.frag"); // Edge Detection Kernel Color Shader
	// Shader screenShader = Shader("shader/vec3_simple_texture.vert", "shader/unif_vec4_tex_post_blur.frag"); // Blur Kernel Color Shader

	unsigned int grassTexture = videoGL.getTexture("assets/window.png", GL_RGBA, false);

	FTime time = FTime();

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();

	shaderMaterial.use();
	shaderMaterial.setVec3Float("dirLight.diffuse", yellow.r, yellow.g, yellow.b);
	shaderMaterial.setVec3Float("dirLight.ambient", yellow.r * 0.2f, yellow.g * 0.2f, yellow.b * 0.2f);
	shaderMaterial.setVec3Float("dirLight.specular", yellow.r, yellow.g, yellow.b);
	shaderMaterial.setVec3Float("dirLight.direction", -0.2f, -1.0f, -0.3f);
	shaderMaterial.setFloat("pointLights[0].constant", 1.0f);
	shaderMaterial.setFloat("pointLights[0].linear", 0.09f);
	shaderMaterial.setFloat("pointLights[0].quadratic", 0.032f);

	grassShader.use();
	grassShader.setInt("ourTexture", 0);

	screenShader.use();
	screenShader.setInt("ourTexture", 0);

	int pointLightCount = 1;
	glm::vec3 pointLights[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
	};

	FColor pointLightColors[] = {
		white
	};

	glm::vec3 cube1Pos = glm::vec3(0.0f, 0.0f, -3.0f);
	glm::vec3 cube2Pos = glm::vec3(-1.0f, 0.0f, -0.0f);
	glm::vec3 floorPos = glm::vec3(0.0f, -0.35f, 0.0f);

	std::vector<glm::vec3> grassPositions;
	grassPositions.push_back(glm::vec3(1.0f, -0.6f, 4.0f));
	grassPositions.push_back(glm::vec3(0.0f, -0.6f, 4.2f));
	grassPositions.push_back(glm::vec3(-1.0f, -0.6f, 4.1f));
	grassPositions.push_back(glm::vec3(2.0f, -0.6f, 4.3f));

	std::string cubePath = "assets/models/cube/MyCube.obj";
	std::string floorPath = "assets/models/floor/MyFloor.obj";

	TinyModel cube1TinyModel = TinyModel(cubePath, false);
	TinyModel floorTinyModel = TinyModel(floorPath, false);

	unsigned int grassVAO = videoGL.prepareTexturedRectangleNormal();
	unsigned int quadVAO = videoGL.prepareTexturedRectangleBuffer();

	std::map<float, glm::vec3> sorted;

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		// First Pass
		videoGL.bindFrameBuffer(frameBuffer);
		videoGL.enableDepthTest();

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		grassShader.use();
		grassShader.setMat4Float("view", view);
		grassShader.setMat4Float("projection", projection);

		shaderMaterial.use();
		shaderMaterial.setMat4Float("view", view);
		shaderMaterial.setVec3Float("viewPos", camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);
		shaderMaterial.setMat4Float("projection", projection);

		glm::mat4 matFloor = glm::mat4(1.0f);
		matFloor = glm::translate(matFloor, floorPos);
		matFloor = glm::scale(matFloor, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matFloor);

		floorTinyModel.Draw(shaderMaterial);

		glm::mat4 matcube1 = glm::mat4(1.0f);
		matcube1 = glm::translate(matcube1, cube1Pos);
		matcube1 = glm::scale(matcube1, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matcube1);

		cube1TinyModel.Draw(shaderMaterial);

		glm::mat4 matcube2 = glm::mat4(1.0f);
		matcube2 = glm::translate(matcube2, cube2Pos);
		matcube2 = glm::scale(matcube2, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matcube2);

		cube1TinyModel.Draw(shaderMaterial);

		grassShader.use();
		glBindVertexArray(grassVAO);
		glBindTexture(GL_TEXTURE_2D, grassTexture);

		videoGL.disableCulling();
		sorted.clear();
		for (int i = 0; i < grassPositions.size(); i++) {
			float distance = glm::distance(camera.getCameraPosition(), grassPositions[i]);
			sorted[distance] = grassPositions[i];
		}

		for (std::map<float, glm::vec3>::reverse_iterator it = sorted.rbegin(); it != sorted.rend(); it++) {
			glm::mat4 grassMat = glm::mat4(1.0f);
			grassMat = glm::translate(grassMat, it->second);
			grassMat = glm::scale(grassMat, glm::vec3(1.0f, 1.0f, 1.0f));
			grassShader.setMat4Float("model", grassMat);
			glDrawArrays(GL_TRIANGLES, 0, 8);
		}
		videoGL.enableCulling();
		glBindVertexArray(0);

		unsigned int emptyFrameBuffer = 0;
		videoGL.bindFrameBuffer(emptyFrameBuffer);
		videoGL.disableDepthTest();

		videoGL.clearFrame();

		screenShader.use();
		glBindVertexArray(quadVAO);
		glBindTexture(GL_TEXTURE_2D, bufferTexture);

		// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		glBindVertexArray(0);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::StencilTests() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();

	Camera camera = Camera(videoGL.getWindow());
	Shader shaderMaterial = Shader("shader/mesh_default.vert", "shader/mesh_default.frag");
	Shader flatMaterial = Shader("shader/mesh_default.vert", "shader/unif_vec4_color.frag");

	FTime time = FTime();

	FColor yellow = Colors::Yellow();
	FColor white = Colors::White();
	FColor green = Colors::Green();

	shaderMaterial.use();
	shaderMaterial.setVec3Float("dirLight.diffuse", yellow.r, yellow.g, yellow.b);
	shaderMaterial.setVec3Float("dirLight.ambient", yellow.r * 0.2f, yellow.g * 0.2f, yellow.b * 0.2f);
	shaderMaterial.setVec3Float("dirLight.specular", yellow.r, yellow.g, yellow.b);
	shaderMaterial.setVec3Float("dirLight.direction", -0.2f, -1.0f, -0.3f);
	shaderMaterial.setFloat("pointLights[0].constant", 1.0f);
	shaderMaterial.setFloat("pointLights[0].linear", 0.09f);
	shaderMaterial.setFloat("pointLights[0].quadratic", 0.032f);

	flatMaterial.use();
	flatMaterial.setVec4Float("myColor", green.r, green.g, green.b, green.a);

	int pointLightCount = 1;
	glm::vec3 pointLights[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
	};

	FColor pointLightColors[] = {
		white
	};

	glm::vec3 cube1Pos = glm::vec3(0.0f, 0.0f, -3.0f);
	glm::vec3 cube2Pos = glm::vec3(-1.0f, 0.0f, -0.0f);
	glm::vec3 floorPos = glm::vec3(0.0f, -0.35f, 0.0f);
	std::string cubePath = "assets/models/cube/MyCube.obj";
	std::string floorPath = "assets/models/floor/MyFloor.obj";

	TinyModel cube1TinyModel = TinyModel(cubePath, false);
	TinyModel floorTinyModel = TinyModel(floorPath, false);

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float deltaTime = time.deltaTime();

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		flatMaterial.use();
		flatMaterial.setMat4Float("view", view);
		flatMaterial.setMat4Float("projection", projection);

		shaderMaterial.use();
		shaderMaterial.setMat4Float("view", view);
		shaderMaterial.setVec3Float("viewPos", camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);
		shaderMaterial.setMat4Float("projection", projection);

		videoGL.disableStencilMask();
		glm::mat4 matFloor = glm::mat4(1.0f);
		matFloor = glm::translate(matFloor, floorPos);
		matFloor = glm::scale(matFloor, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matFloor);

		floorTinyModel.Draw(shaderMaterial);

		videoGL.stencilWrite();
		glm::mat4 matcube1 = glm::mat4(1.0f);
		matcube1 = glm::translate(matcube1, cube1Pos);
		matcube1 = glm::scale(matcube1, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matcube1);

		cube1TinyModel.Draw(shaderMaterial);

		glm::mat4 matcube2 = glm::mat4(1.0f);
		matcube2 = glm::translate(matcube2, cube2Pos);
		matcube2 = glm::scale(matcube2, glm::vec3(1.0f, 1.0f, 1.0f));
		shaderMaterial.setMat4Float("model", matcube2);

		cube1TinyModel.Draw(shaderMaterial);

		videoGL.stencilEnd(true);
		flatMaterial.use();
		glm::mat4 matFlatCube1 = glm::mat4(1.0f);
		matFlatCube1 = glm::translate(matFlatCube1, cube1Pos);
		matFlatCube1 = glm::scale(matFlatCube1, glm::vec3(1.2f, 1.2f, 1.2f));
		flatMaterial.setMat4Float("model", matFlatCube1);

		cube1TinyModel.Draw(flatMaterial);

		//glm::mat4 matFlatCube2 = glm::mat4(1.0f);
		//matFlatCube2 = glm::translate(matFlatCube2, cube2Pos);
		//matFlatCube2 = glm::scale(matFlatCube2, glm::vec3(1.2f, 1.2f, 1.2f));
		//flatMaterial.setMat4Float("model", matFlatCube2);

		//cube1TinyModel.Draw(flatMaterial);
		videoGL.stencilClear();

		glBindVertexArray(0);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::Models() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();

	Camera camera = Camera(videoGL.getWindow());

	Shader shaderMaterial = Shader("shader/mesh_default.vert", "shader/mesh_default.frag");
	Shader shaderColor01 = Shader("shader/vec3_texture.vert", "shader/unif_vec4_tex.frag");
	Shader skyboxShader = Shader("shader/cubemap.vert", "shader/cubemap.frag");

	glm::vec3 yellowColor = glm::vec3(1.0f, 1.0f, 0.92f);
	glm::vec3 whiteColor = glm::vec3(1.0f, 1.0f, 1.0f);
	glm::vec3 redColor = glm::vec3(1.0f, 0.5f, 0.5f);

	std::vector<std::string> skyboxTexturePath = {
	"assets/skybox/right.jpg",
	"assets/skybox/left.jpg",
	"assets/skybox/top.jpg",
	"assets/skybox/bottom.jpg",
	"assets/skybox/front.jpg",
	"assets/skybox/back.jpg"
	};
	unsigned int skyboxTexure = videoGL.getCubeMapTexture(skyboxTexturePath, GL_RGB, false);

	shaderMaterial.use();
	shaderMaterial.setVec3Float("dirLight.diffuse", yellowColor.x, yellowColor.y, yellowColor.z);
	shaderMaterial.setVec3Float("dirLight.ambient", yellowColor.x * 0.2f, yellowColor.y * 0.2f, yellowColor.z * 0.2f);
	shaderMaterial.setVec3Float("dirLight.specular", yellowColor.x, yellowColor.y, yellowColor.z);
	shaderMaterial.setVec3Float("dirLight.direction", -0.2f, -1.0f, -0.3f);
	shaderMaterial.setInt("skybox", 11);

	skyboxShader.use();
	skyboxShader.setInt("skybox", 0);

	float deltaTime = 0.0f;
	float lastFrame = 0.0f;
	float angle = 20.0f;
	float lightAngle = 20.0f;

	int pointLightCount = 4;
	glm::vec3 pointLights[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
		glm::vec3(2.3f, -3.3f, -4.0f),
		glm::vec3(-4.0f,  2.0f, -12.0f),
		glm::vec3(0.0f,  0.0f, -3.0f)
	};

	glm::vec3 pointLightColors[] = {
		whiteColor,
		whiteColor,
		whiteColor,
		whiteColor
	};

	shaderColor01.use();
	shaderColor01.setVec3Float("myColor", pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
	shaderColor01.setVec3Float("myLight", pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);

	glm::vec3 backPackPos = glm::vec3(0.0f, -2.0f, -3.0f);
	//std::string backPackPath = "assets/models/car/Alfa_Romeo_Giulia_GTAm.obj";
	std::string backPackPath = "assets/models/backpack/backpack.obj";
	//std::string backPackPath = "assets/models/mustang/mustang.obj";
	//std::string backPackPath = "assets/models/sponza/sponza.obj";

	std::string sunPath = "assets/models/EggSun/egg_sun.obj";
	TinyModel backPack = TinyModel(backPackPath, true);
	TinyModel sun = TinyModel(sunPath, false);

	unsigned int skyboxVAO = videoGL.prepareCubeMapCube();

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_RIGHT)) {
			angle += 50.0f * deltaTime;
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_LEFT)) {
			angle -= 50.0f * deltaTime;
		}

		videoGL.clearFrame();

		const float radius = 5.0f;
		float lightX = sin(glfwGetTime()) * radius;
		float lightZ = cos(glfwGetTime()) * radius;

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		shaderColor01.use();
		shaderColor01.setMat4Float("view", view);
		shaderColor01.setMat4Float("projection", projection);

		glActiveTexture(GL_TEXTURE11);
		shaderMaterial.setInt("skybox", 11);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexure);

		glm::mat4 pointTinyModel = glm::mat4(1.0f);
		pointTinyModel = glm::translate(pointTinyModel, glm::vec3(lightX, pointLights[0].y, lightZ));
		pointTinyModel = glm::scale(pointTinyModel, glm::vec3(0.2f));

		shaderColor01.setVec3Float("myColor", pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
		shaderColor01.setVec3Float("myLight", pointLightColors[0].x, pointLightColors[0].y, pointLightColors[0].z);
		shaderColor01.setMat4Float("model", pointTinyModel);

		// draw point light
		sun.Draw(shaderColor01);

		shaderMaterial.use();
		shaderMaterial.setFloat("time", glfwGetTime());
		shaderMaterial.setMat4Float("view", view);
		shaderMaterial.setVec3Float("viewPos", camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);
		shaderMaterial.setMat4Float("projection", projection);

		for (int i = 0; i < pointLightCount; i++) {
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].position", lightX, pointLights[i].y, lightZ);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].ambient", pointLightColors[i].x * 0.2f, pointLightColors[i].y * 0.2f, pointLightColors[i].z * 0.2f);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].diffuse", pointLightColors[i].x, pointLightColors[i].y, pointLightColors[i].z);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].specular", pointLightColors[i].x, pointLightColors[i].y, pointLightColors[i].z);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].constant", 1.0f);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].linear", 0.09f);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].quadratic", 0.032f);
		}

		glm::mat4 matBackPack = glm::mat4(1.0f);
		matBackPack = glm::translate(matBackPack, backPackPos);
		matBackPack = glm::scale(matBackPack, glm::vec3(1.0f, 1.0f, 1.0f));
		matBackPack = glm::rotate(matBackPack, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
		shaderMaterial.setMat4Float("model", matBackPack);

		backPack.Draw(shaderMaterial);

		glBindVertexArray(0);

		// SKYBOX START
		glDepthFunc(GL_LEQUAL);
		skyboxShader.use();
		glm::mat4 fixView = glm::mat4(glm::mat3(view));
		skyboxShader.setMat4Float("view", fixView);
		skyboxShader.setMat4Float("projection", projection);
		glBindVertexArray(skyboxVAO);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxTexure);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glDepthFunc(GL_LESS);
		// SKYBOX END

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::Lighting() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();

	Camera camera = Camera(videoGL.getWindow());

	unsigned int texture = videoGL.getTexture("assets/container.jpg", GL_RGB, false);
	unsigned int texture2 = videoGL.getTexture("assets/awesomeface.png", GL_RGBA, true);
	unsigned int texture3 = videoGL.getTexture("assets/container2.png", GL_RGBA, false);
	unsigned int specularTexture3 = videoGL.getTexture("assets/container2_specular.png", GL_RGBA, false);
	unsigned int emissionTexture3 = videoGL.getTexture("assets/matrix.jpg", GL_RGB, false);

	Shader shaderColor01 = Shader("shader/vec3_texture.vert", "shader/unif_vec4_light_color.frag");
	Shader shaderMaterial = Shader("shader/mesh_default.vert", "shader/unif_mat_map_light.frag");

	unsigned int VAOTextRect = videoGL.prepareTexturedNormalCube();
	unsigned int VAOPointLight = videoGL.prepareTexturedCube();

	glm::vec3 whiteColor = glm::vec3(1.0f, 1.0f, 1.0f);
	glm::vec3 yellowColor = glm::vec3(1.0f, 1.0f, 0.8f);
	glm::vec3 cyanColor = glm::vec3(0.6f, 1.0f, 1.0f);
	glm::vec3 pinkColor = glm::vec3(1.0f, 0.6f, 0.6f);
	glm::vec3 bluishColor = glm::vec3(0.0f, 0.6f, 1.0f);

	shaderColor01.use();
	shaderColor01.setVec3Float("myColor", 1.0f, 1.0f, 1.0f);
	shaderColor01.setVec3Float("myLight", whiteColor.x, whiteColor.y, whiteColor.z);

	shaderMaterial.use();
	shaderMaterial.setInt("material.diffuse", 0);
	shaderMaterial.setInt("material.specular", 1);
	shaderMaterial.setFloat("material.shineness", 32.0f);
	shaderMaterial.setVec3Float("dirLight.diffuse", yellowColor.x, yellowColor.y, yellowColor.z);
	shaderMaterial.setVec3Float("dirLight.ambient", yellowColor.x * 0.2f, yellowColor.y * 0.2f, yellowColor.z * 0.2f);
	shaderMaterial.setVec3Float("dirLight.specular", yellowColor.x, yellowColor.y, yellowColor.z);
	shaderMaterial.setVec3Float("dirLight.direction", -0.2f, -1.0f, -0.3f);
	shaderMaterial.setFloat("spotLight.cutoff", glm::cos(glm::radians(12.5f)));
	shaderMaterial.setFloat("spotLight.outerCutOff", glm::cos(glm::radians(17.5f)));

	float deltaTime = 0.0f;
	float lastFrame = 0.0f;
	float angle = 20.0f;

	int pointLightCount = 4;

	glm::vec3 pointLights[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
		glm::vec3(2.3f, -3.3f, -4.0f),
		glm::vec3(-4.0f,  2.0f, -12.0f),
		glm::vec3(0.0f,  0.0f, -3.0f)
	};

	glm::vec3 pointLightColors[] = {
		cyanColor,
		pinkColor,
		whiteColor,
		glm::vec3(0.0f,1.0f,0.0f)
	};

	glm::vec3 cubePositions[] = {
		glm::vec3(2.0f,  -1.0f,  -2.0f),
		glm::vec3(2.0f,  5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f,  3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f,  2.0f, -2.5f),
		glm::vec3(1.5f,  0.2f, -1.5f),
		glm::vec3(-1.3f,  1.0f, -1.5f)
	};

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE)) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		shaderColor01.use();
		shaderColor01.setMat4Float("view", view);
		shaderColor01.setMat4Float("projection", projection);
		for (int i = 0; i < pointLightCount; i++) {
			glm::mat4 pointTinyModel = glm::mat4(1.0f);
			pointTinyModel = glm::translate(pointTinyModel, pointLights[i]);
			pointTinyModel = glm::scale(pointTinyModel, glm::vec3(0.2f));

			shaderColor01.setVec3Float("myColor", pointLightColors[i].x, pointLightColors[i].y, pointLightColors[i].z);
			shaderColor01.setVec3Float("myLight", pointLightColors[i].x, pointLightColors[i].y, pointLightColors[i].z);
			shaderColor01.setMat4Float("model", pointTinyModel);

			glBindVertexArray(VAOPointLight);

			glDrawArrays(GL_TRIANGLES, 0, 36);
			glBindVertexArray(0);
		}

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture3);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, specularTexture3);

		shaderMaterial.use();
		shaderMaterial.setVec3Float("viewPos", camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);
		shaderMaterial.setMat4Float("view", view);
		shaderMaterial.setMat4Float("projection", projection);

		shaderMaterial.setVec3Float("spotLight.position", camera.getCameraPosition().x, camera.getCameraPosition().y, camera.getCameraPosition().z);
		shaderMaterial.setVec3Float("spotLight.direction", camera.getDirection().x, camera.getDirection().y, camera.getDirection().z);
		shaderMaterial.setVec3Float("spotLight.ambient", bluishColor.x * 0.2f, bluishColor.y * 0.2f, bluishColor.z * 0.2f);
		shaderMaterial.setVec3Float("spotLight.diffuse", bluishColor.x, bluishColor.y, bluishColor.z);
		shaderMaterial.setVec3Float("spotLight.specular", bluishColor.x, bluishColor.y, bluishColor.z);

		for (int i = 0; i < pointLightCount; i++) {
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].position", pointLights[i].x, pointLights[i].y, pointLights[i].z);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].ambient", pointLightColors[i].x * 0.2f, pointLightColors[i].y * 0.2f, pointLightColors[i].z * 0.2f);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].diffuse", pointLightColors[i].x, pointLightColors[i].y, pointLightColors[i].z);
			shaderMaterial.setVec3Float("pointLights[" + std::to_string(i) + "].specular", pointLightColors[i].x, pointLightColors[i].y, pointLightColors[i].z);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].constant", 1.0f);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].linear", 0.09f);
			shaderMaterial.setFloat("pointLights[" + std::to_string(i) + "].quadratic", 0.032f);
		}

		for (int i = 0; i < 10; i++) {
			glm::mat4 matCubeTinyModel = glm::mat4(1.0f);
			matCubeTinyModel = glm::translate(matCubeTinyModel, cubePositions[i]);
			matCubeTinyModel = glm::rotate(matCubeTinyModel, (float)glfwGetTime() * glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
			shaderMaterial.setMat4Float("model", matCubeTinyModel);

			glBindVertexArray(VAOTextRect);
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}

		glBindVertexArray(0);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

void AppRun::GettingStartedApp() {
	VideoGL videoGL = VideoGL();
	videoGL.videoInit();

	Camera camera = Camera(videoGL.getWindow());

	// Debug log number of vertex attribs from GPU
	int nrAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
	std::cout << "Max numbers of vertex attribs is " << nrAttributes << std::endl;

	unsigned int texture = videoGL.getTexture("assets/container.jpg", GL_RGB, false);
	unsigned int texture2 = videoGL.getTexture("assets/awesomeface.png", GL_RGBA, true);

	Shader shaderTexture01 = Shader("shader/vec3_texture.vert", "shader/unif_vec4_multi_tex.frag");

	unsigned int VAOTextRect = videoGL.prepareTexturedCube();

	shaderTexture01.use();
	shaderTexture01.setInt("ourTexture", 0);
	shaderTexture01.setInt("ourTexture2", 1);

	float timeValue = 0.0f;
	float greenValue = 0.0f;
	float offsetValue = 0.0f;
	float alphaValue = 0.2f;

	glm::vec3 cubePositions[] = {
		glm::vec3(0.0f,  0.0f,  0.0f),
		glm::vec3(2.0f,  5.0f, -15.0f),
		glm::vec3(-1.5f, -2.2f, -2.5f),
		glm::vec3(-3.8f, -2.0f, -12.3f),
		glm::vec3(2.4f, -0.4f, -3.5f),
		glm::vec3(-1.7f,  3.0f, -7.5f),
		glm::vec3(1.3f, -2.0f, -2.5f),
		glm::vec3(1.5f,  2.0f, -2.5f),
		glm::vec3(1.5f,  0.2f, -1.5f),
		glm::vec3(-1.3f,  1.0f, -1.5f)
	};

	float deltaTime = 0.0f;
	float lastFrame = 0.0f;

	while (!glfwWindowShouldClose(videoGL.getWindow())) {
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;

		float cameraSpeed = 2.5f * deltaTime;

		if (getInput(videoGL.getWindow(), GLFW_KEY_ESCAPE) && alphaValue < 1.0f) {
			videoGL.closeWindow();
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_UP) && alphaValue < 1.0f) {
			alphaValue += 0.5 * deltaTime;
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_DOWN) && alphaValue > 0.0f) {
			alphaValue -= 0.5 * deltaTime;
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_W)) {
			camera.moveForward(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_S)) {
			camera.moveBackwards(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_A)) {
			camera.moveLeft(deltaTime);
		}
		if (getInput(videoGL.getWindow(), GLFW_KEY_D)) {
			camera.moveRight(deltaTime);
		}

		videoGL.clearFrame();

		shaderTexture01.use();
		shaderTexture01.setFloat("alpha", alphaValue);

		glm::mat4 view = camera.calculateView();
		glm::mat4 projection = camera.calculateProjection();

		shaderTexture01.setMat4Float("view", view);
		shaderTexture01.setMat4Float("projection", projection);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);
		glBindVertexArray(VAOTextRect);

		for (int i = 0; i < 10; i++) {
			float rotTime = 1.0f;
			if (i % 3 == 0) {
				rotTime = (float)glfwGetTime();
			}
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, cubePositions[i]);
			float angle = 20.0f * (i + 1);
			model = glm::rotate(model, rotTime * glm::radians(angle), glm::vec3(0.5f, 1.0f, 0.0f));

			shaderTexture01.setMat4Float("model", model);
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}

		glBindVertexArray(0);

		videoGL.endPollFrame();
	}

	videoGL.terminate();
}

bool AppRun::getInput(GLFWwindow* p_window, int key) {
	if (glfwGetKey(p_window, key) == GLFW_PRESS) {
		return true;
	}

	return false;
}