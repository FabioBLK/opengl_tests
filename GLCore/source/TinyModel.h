#pragma once

//#define TINYOBJLOADER_IMPLEMENTATION
#include "tinyobjloader/tiny_obj_loader.h"

#include "Shader.h"
#include "Mesh.h"
#include "IModel.h"

#include "stb_image.h"

class TinyModel : public IModel
{
public:
	TinyModel(std::string path, bool p_flipTexure);
	virtual void DrawModel(Shader& shader);
	virtual void Draw(Shader& shader);
	virtual void DrawInstanced(Shader& shader, const unsigned int p_amount);
	virtual void SetInstances(const unsigned int p_amount, glm::mat4* p_matrices);
	virtual void SetPosition(const glm::vec3* p_position);
	virtual void SetScale(const glm::vec3* p_scale);
private:
	std::vector<Texture> textures_loaded;
	std::unordered_map<unsigned int, Material> m_materials_loaded;
	std::vector<Mesh> m_meshes;
	std::string m_directory;

	void loadModel(std::string path);
	void processModel(tinyobj::attrib_t* p_attrib, std::vector<tinyobj::shape_t>& p_shapes, std::vector<tinyobj::material_t>& p_materials);
	Mesh processMesh(tinyobj::attrib_t* p_attrib, tinyobj::mesh_t* p_mesh, std::vector<tinyobj::material_t>& p_materials);
	Texture loadMaterialTexture(tinyobj::material_t* p_mat, MeshTextureType meshType);
	unsigned int TextureFromFile(std::string path, std::string directory);
	unsigned int WhiteTexture();
	unsigned int m_whiteTextureId;
	bool m_flipTexture;
	glm::mat4 m_modelMatrix;
	glm::vec3 m_position;
	glm::vec3 m_scale;
};

