#pragma once
#include <GLFW/glfw3.h>

class FTime
{
public:
	FTime();

	/// <summary>
	/// Should be called inside the game loop
	/// </summary>
	/// <returns></returns>
	float deltaTime();

	/// <summary>
	/// Returns the current time
	/// </summary>
	/// <returns></returns>
	float getTime();
private:
	float m_deltaTime;
	float m_lastFrame;
	float m_currentFrame;
};

