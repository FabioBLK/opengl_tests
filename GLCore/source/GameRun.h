#pragma once

#include "FLogs.h"
#include "VideoGL.h"
#include "FInput.h"
#include "FCamera.h"
#include "Shader.h"
#include "FTime.h"
#include "Colors.h"
#include "FGui.h"
#include "FLight.h"
#include "FModel.h"
#include "Files.h"

class GameRun
{
public:
	GameRun();
	void MainScene();
	void InputCameraMove(FCamera* p_camera, FInput* p_input, float p_deltaTime);
	glm::vec3 InputLightMove(glm::vec3 p_lastPos, FCamera* p_camera, FInput* p_input, float p_deltaTime);
	void InputJoyCameraMove(FCamera* p_camera, FInput* p_input, float p_deltaTime);
	void InputJoyLightMove(glm::vec3* p_lightPos, FInput* p_input, FCamera* p_camera, float p_deltaTime);
	void InputCloseGame(FInput* p_input);
	void InputActivateMouse(FInput* p_input, FCamera* p_camera);
	void ButtonClick();
	void CloseGame();

private:
	VideoGL m_videoGL;
	FTime m_time;

	bool m_mouseEnabled = true;
};

