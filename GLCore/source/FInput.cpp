#include "FInput.h"

FInput::FInput(GLFWwindow* p_window) {
	glfwSetWindowUserPointer(p_window, this);

	auto mouseCallback = [](GLFWwindow* window, double xpos, double ypos)
	{
		static_cast<FInput*>(glfwGetWindowUserPointer(window))->mouse_callback(window, xpos, ypos);
	};

	auto scrollCallback = [](GLFWwindow* window, double xoffset, double yoffset)
	{
		static_cast<FInput*>(glfwGetWindowUserPointer(window))->scroll_callback(window, xoffset, yoffset);
	};

	auto keyCallback = [](GLFWwindow* window, int key, int scanCode, int action, int mods)
	{
		static_cast<FInput*>(glfwGetWindowUserPointer(window))->key_callback(window, key, scanCode, action, mods);
	};

	glfwSetCursorPosCallback(p_window, mouseCallback);
	glfwSetScrollCallback(p_window, scrollCallback);
	glfwSetKeyCallback(p_window, keyCallback);

	if (glfwJoystickIsGamepad(GLFW_JOYSTICK_1))
	{
		const char* name = glfwGetGamepadName(GLFW_JOYSTICK_1);
		std::string nameStr = name;
		std::string message = "FINPUT::INFO::Joystick connected name is: " + nameStr;
		FLogs::shared_instance().Log(message);
	}
}

void FInput::SetInputExecutors(IInput* p_input) {
	m_inputList.push_back(p_input);
}

void FInput::RemoveInputExecutors(IInput* p_input) {
	// TODO : NEEDS TESTING
	int index = -1;
	for (int i = 0; i < m_inputList.size(); i++)
	{
		if (p_input == m_inputList[i]) {
			index = i;
			break;
		}
	}

	if (index != -1) {
		m_inputList.erase(m_inputList.begin() + index);
	}
}

bool FInput::GetInput(int key, GLFWwindow* p_window) {
	if (glfwGetKey(p_window, key) == GLFW_PRESS) {
		return true;
	}
	return false;
}

bool FInput::GetInputDown(int key) {
	if (m_pressedKeys.find(key) != m_pressedKeys.end()) {
		return true;
	}
	return false;
}

void FInput::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (action == GLFW_PRESS) {
		//std::string message = "PRESS::key = " + std::to_string(key) + " scancode = " + std::to_string(scancode);
		//FLogs::shared_instance().Log(message);
		if (m_pressedKeys.find(key) == m_pressedKeys.end()) {
			m_pressedKeys.insert(key);
		}
	}
	else if (action == GLFW_REPEAT || action == GLFW_RELEASE)
	{
		if (m_pressedKeys.find(key) != m_pressedKeys.end()) {
			m_pressedKeys.erase(key);
		}
	}
}

void FInput::PollEvents() {
	m_pressedKeys.clear();
	glfwPollEvents();
	
	m_lastState = m_currentState;
	glfwGetGamepadState(GLFW_JOYSTICK_1, &m_currentState);
}

bool FInput::GetButtonDown(int key) {
	if (m_currentState.buttons[key] == GLFW_PRESS && m_lastState.buttons[key] == GLFW_RELEASE) {
		std::string message = "INPUT::PRESS Button = " + std::to_string(key);
		//FLogs::shared_instance().Log(message);
		return true;
	}
	
	return false;
}

bool FInput::GetButton(int key) {
	if (m_currentState.buttons[key] == GLFW_PRESS) {
		std::string message = "INPUT::PRESS Button = " + std::to_string(key);
		//FLogs::shared_instance().Log(message);
		return true;
	}
	
	return false;
}

float FInput::GetAxis(int p_axis) {
	return m_currentState.axes[p_axis];
}

glm::vec2 FInput::GetStickAxis(int p_horizontalAxis, int p_verticalAxis) {
	glm::vec2 axisInput = glm::vec2(m_currentState.axes[p_horizontalAxis], m_currentState.axes[p_verticalAxis]);
	float magnitude = glm::length2(axisInput);
	if (magnitude < m_joyDeadZone) {
		return glm::vec2(0.0f);
	}

	glm::vec2 normalized = glm::normalize(axisInput);
	return normalized * ((magnitude - m_joyDeadZone) / (1 - m_joyDeadZone));
}

void FInput::mouse_callback(GLFWwindow* window, double xpos, double ypos) {
	for (int i = 0; i < m_inputList.size(); i++) {
		m_inputList[i]->MousePosition(xpos, ypos);
	}
}

void FInput::scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
	for (int i = 0; i < m_inputList.size(); i++) {
		m_inputList[i]->MouseScroll(xoffset, yoffset);
	}
}
