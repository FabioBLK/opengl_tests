#include "GameRun.h"

GameRun::GameRun() {
	m_videoGL.videoInit();
	m_videoGL.enableBlend();
	m_videoGL.enableCulling();

	FGui::Init(m_videoGL.getWindow());
}

void GameRun::MainScene() {
	FLogs::shared_instance().Log("STARING MAIN SCENE");

	FCamera camera = FCamera();
	Shader shader01 = Shader(PATH_DEFAULT_VERTEX_SHADER.c_str(), PATH_DEFAULT_FRAG_SHADER.c_str());
	Shader unlitShader = Shader(PATH_SIMPLE_DEFAULT_VERTEX_SHADER.c_str(), PATH_UNIFORM_COLOR_FRAG_SHADER.c_str());
	FTime time = FTime();
	FInput input = FInput(m_videoGL.getWindow());
	input.SetInputExecutors(&camera);

	FColor dirLightColor = Colors::White();
	FColor white = Colors::White();
	FColor green = Colors::Green();
	FColor dimWhite = { white.r * 0.2f,white.g * 0.2f,white.b * 0.2f, 1.0f };

	std::string cubePath = PATH_MESH_CUBE;
	std::string floorPath = PATH_MESH_FLOOR;
	std::string backPackPath = PATH_MESH_BACKPACK;

	FModel fCubemodel = FModel(cubePath, false);
	FModel fBackpackModel = FModel(backPackPath, true);
	IModel* backpackModel = fBackpackModel.Model();
	IModel* lightCube = fCubemodel.Model();

	float cubeSize = 1.0f;
	int selectedItem = 0;

	glm::vec3 dirLightPos = glm::vec3(1.0f, 0.0f, 3.0f);
	FLight dirLight = FLight(FLightType::DIRECTIONAL);

	glm::vec3 pointLightPos = glm::vec3(2.0f, 0.0f, 4.0f);
	FLight pointLight = FLight(FLightType::POINT);

	while (!glfwWindowShouldClose(m_videoGL.getWindow())) {
		float currentTime = time.getTime();
		float deltaTime = time.deltaTime();

		input.PollEvents();
		InputCloseGame(&input);
		if (selectedItem == 0) {
			InputJoyCameraMove(&camera, &input, deltaTime);
			InputCameraMove(&camera, &input, deltaTime);
		}
		if (selectedItem == 1) {
			//InputJoyLightMove(&pointLightPos, &input, &camera, deltaTime);
			pointLightPos = InputLightMove(pointLightPos , &camera, &input, deltaTime);
		}
		InputActivateMouse(&input, &camera);

		FGui::FrameStart(&input);
		FGui::SettingsWindow();
		FGui::SceneWindow(&cubeSize, &dimWhite, &dirLightPos, &dirLightColor, &selectedItem, [&] { ButtonClick(); });

		unlitShader.use();
		unlitShader.setVec4Float("myColor", dirLightColor.r, dirLightColor.g, dirLightColor.b, 1.0f);

		dirLight.Calculate(&shader01, &dimWhite, &dirLightPos);
		pointLight.Calculate(&shader01, &dirLightColor, &pointLightPos);
		m_videoGL.clearFrame();
		camera.render(shader01);
		camera.render(unlitShader);

		glm::vec3 cubePos = glm::vec3(0.0f, 0.0f, -1.0f);
		glm::vec3 cubevSize = glm::vec3(cubeSize);
		backpackModel->SetPosition(&cubePos);
		backpackModel->SetScale(&cubevSize);
		
		backpackModel->DrawModel(shader01);

		glm::vec3 pointLightSize = glm::vec3(0.25f);
		lightCube->SetPosition(&pointLightPos);
		lightCube->SetScale(&pointLightSize);

		lightCube->DrawModel(unlitShader);

		FGui::Render();
		m_videoGL.endFrame();
	}

	FGui::Terminate();
	m_videoGL.terminate();
}

void GameRun::InputCameraMove(FCamera* p_camera, FInput* p_input, float p_deltaTime) {
	if (!m_mouseEnabled) {
		return;
	}

	float xAxis = 0.0f;
	float yAxis = 0.0f;

	if (p_input->GetInput(GLFW_KEY_W, m_videoGL.getWindow())) {
		yAxis = -1.0f;
	}
	if (p_input->GetInput(GLFW_KEY_S, m_videoGL.getWindow())) {
		yAxis = 1.0f;
	}
	if (p_input->GetInput(GLFW_KEY_A, m_videoGL.getWindow())) {
		xAxis = -1.0f;
	}
	if (p_input->GetInput(GLFW_KEY_D, m_videoGL.getWindow())) {
		xAxis = 1.0f;
	}

	p_camera->moveXAxis(xAxis * p_deltaTime);
	p_camera->moveZAxis(yAxis * p_deltaTime);
}

glm::vec3 GameRun::InputLightMove(glm::vec3 p_lastPos, FCamera* p_camera, FInput* p_input, float p_deltaTime) {
	float xAxis = 0.0f;
	float zAxis = 0.0f;

	if (p_input->GetInput(GLFW_KEY_W, m_videoGL.getWindow())) {
		zAxis = 1.0f * p_deltaTime;
	}
	if (p_input->GetInput(GLFW_KEY_S, m_videoGL.getWindow())) {
		zAxis = -1.0f * p_deltaTime;
	}
	if (p_input->GetInput(GLFW_KEY_A, m_videoGL.getWindow())) {
		xAxis = -1.0f * p_deltaTime;
	}
	if (p_input->GetInput(GLFW_KEY_D, m_videoGL.getWindow())) {
		xAxis = 1.0f * p_deltaTime;
	}

	glm::vec3 camForward = p_camera->getCameraFront();
	camForward.y = 0;
	glm::vec3 camRight = p_camera->getCameraRight();
	camRight.y = 0;

	glm::vec3 position = camForward * zAxis + camRight * xAxis;
	position = p_lastPos + position;

	return position;
}

void GameRun::InputJoyCameraMove(FCamera* p_camera, FInput* p_input, float p_deltaTime) {
	glm::vec2 rightJoyAxis = p_input->GetStickAxis(GLFW_GAMEPAD_AXIS_RIGHT_X, GLFW_GAMEPAD_AXIS_RIGHT_Y);
	p_camera->moveCameraView(rightJoyAxis.x, rightJoyAxis.y, p_deltaTime);

	glm::vec2 leftJoyAxis = p_input->GetStickAxis(GLFW_GAMEPAD_AXIS_LEFT_X, GLFW_GAMEPAD_AXIS_LEFT_Y);
	p_camera->moveXAxis(leftJoyAxis.x * p_deltaTime);
	p_camera->moveZAxis(leftJoyAxis.y * p_deltaTime);
}

void GameRun::InputJoyLightMove(glm::vec3* p_lightPos, FInput* p_input, FCamera* p_camera, float p_deltaTime) {
	glm::vec2 rightJoyAxis = p_input->GetStickAxis(GLFW_GAMEPAD_AXIS_RIGHT_X, GLFW_GAMEPAD_AXIS_RIGHT_Y);
	//IModel->moveCameraView(rightJoyAxis.x, rightJoyAxis.y, p_deltaTime);

	glm::vec2 leftJoyAxis = p_input->GetStickAxis(GLFW_GAMEPAD_AXIS_LEFT_X, GLFW_GAMEPAD_AXIS_LEFT_Y);
	//float xAxis = (p_lightPos->x + leftJoyAxis.x) * 10 * p_deltaTime;
	//float yAxis = (p_lightPos->y + leftJoyAxis.y) * 10 * p_deltaTime;
	glm::vec3 movement = glm::vec3(leftJoyAxis.x, 0, 0);

	glm::vec3 moveX = p_camera->getCameraRight() * movement;
	std::string log = "POSITION: " + std::to_string(p_camera->getCameraRight().x) + " - " + std::to_string(p_camera->getCameraRight().y) + " - " + std::to_string(p_camera->getCameraRight().z);
	FLogs::shared_instance().Log(log);
	//glm::vec3 moveZ = p_camera->getCameraFront() * movement;

	glm::vec3 position = (moveX);

	p_lightPos->x += position.x * 5 * p_deltaTime;
	//p_lightPos->y += position.y * 5 * p_deltaTime;
	//p_lightPos->z += position.z * 5 * p_deltaTime;
}

void GameRun::InputCloseGame(FInput* p_input) {
	if (p_input->GetInput(GLFW_KEY_ESCAPE, m_videoGL.getWindow()) || p_input->GetButtonDown(GLFW_GAMEPAD_BUTTON_BACK)) {
		CloseGame();
	}
}

void GameRun::InputActivateMouse(FInput* p_input, FCamera* p_camera) {
	if (p_input->GetInputDown(GLFW_KEY_SPACE)) {
		m_mouseEnabled = !m_mouseEnabled;
		m_videoGL.changeMouseVisibility(m_mouseEnabled);
		p_camera->setMovement(m_mouseEnabled);
	}
}

void GameRun::ButtonClick() {
	FLogs::shared_instance().Log("Callback here");
}

void GameRun::CloseGame() {
	m_videoGL.closeWindow();
}
