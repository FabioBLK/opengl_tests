#pragma once
#include "FLogs.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include <vector>
#include <glm/glm.hpp>

const int WIDTH_RESOLUTION = 1280;
const int HEIGHT_RESOLUTION = 720;
const int MSAA_SAMPLES = 4;

static int MAX_FRAMERATE = 60;

class VideoGL
{
public:
	VideoGL();
	int videoInit();
	void bindFrameBuffer(GLuint& p_frameBuffer);
	void swapFrameBuffer(GLuint& p_readFrameBuffer, GLuint& p_writeFrameBuffer);
	void enableMSAA();
	void enableBlend();
	void enableCulling();
	void disableCulling();
	void stencilWrite();
	void disableStencilMask();
	void enableDepthTest();
	void disableDepthTest();
	void stencilClear();
	void stencilEnd(bool p_disableDepth);
	GLFWwindow* getWindow();
	void clearFrame();
	void changeMouseVisibility(bool p_enable);
	void endFrame();
	void endPollFrame();
	void closeWindow();
	void terminate();
	unsigned int getFrameBufferTexture(unsigned int p_bufferTexture);
	unsigned int getFrameBufferMultiSampleTexture(unsigned int p_bufferTexture);
	unsigned int getTexture(std::string p_filePath, GLenum p_format, bool p_flip);
	unsigned int getCubeMapTexture(std::vector<std::string>& p_filesPath, GLenum p_format, bool p_flip);
	unsigned int getNullTexture(int p_width, int p_height);
	unsigned int getNullMultiSampleTexture(int p_width, int p_height, int p_samples);
	unsigned int prepareTriangle(float vert[9]);
	unsigned int prepareVertexShaderTriangle();
	unsigned int prepareTexturedNormalCube();
	unsigned int prepareTwoTriangles();
	unsigned int prepareRectangle();
	unsigned int prepareTexturedRectangleNormal();
	unsigned int prepareTexturedRectangle();
	unsigned int prepareTexturedRectangleBuffer();
	unsigned int prepareTexturedCube();
	unsigned int prepareCubeMapCube();
	unsigned int prepareVertexPoints();
	unsigned int prepareColorRectangle();
	unsigned int prepareColorRectangleOffset();
private:
	GLFWwindow* m_window = nullptr;
};

