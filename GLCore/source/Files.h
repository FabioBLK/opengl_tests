#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <dirent.h>
#include <queue>

#if defined _MSC_VER
#include <direct.h>
#else
#include <sys/types.h>
#include <sys/stat.h>
#endif

#if defined _MSC_VER
const std::string PATH_DEFAULT_VERTEX_SHADER = "romfs/shader/mesh_default.vert";
const std::string PATH_DEFAULT_FRAG_SHADER = "romfs/shader/mesh_default.frag";
const std::string PATH_UNIFORM_COLOR_FRAG_SHADER = "romfs/shader/unif_vec4_color.frag";
const std::string PATH_SIMPLE_DEFAULT_VERTEX_SHADER = "romfs/shader/simple_default.vert";
const std::string PATH_MESH_CUBE = "romfs/models/cube/MyCube.obj";
const std::string PATH_MESH_FLOOR = "romfs/models/floor/MyFloor.obj";
const std::string PATH_MESH_BACKPACK = "romfs/models/backpack/backpack.obj";
#else
const std::string PATH_DEFAULT_VERTEX_SHADER = "romfs:/shader/mesh_default.vert";
const std::string PATH_DEFAULT_FRAG_SHADER = "romfs:/shader/mesh_default.frag";
const std::string PATH_MESH_CUBE = "romfs:/models/cube/MyCube.obj";
const std::string PATH_MESH_FLOOR = "romfs:/models/floor/MyFloor.obj";
const std::string PATH_MESH_BACKPACK = "romfs:/models/backpack/backpack.obj";
#endif

struct FilesData
{
    std::string name;
    bool isFolder;
    int type;
    int fileSize;
    long int mtime;
    bool selectedToTransfer;
};

class Files
{
private:

public:
    Files();
    ~Files();
    bool getDirData(std::string p_path, std::vector<FilesData>* p_data);
    bool getDirDataQueue(std::string p_path, std::queue<FilesData>* p_data);
    bool isDirValid(std::string p_path);
    bool createDir(std::string p_path);
    int getFileSize(std::string p_path);
    bool loadFileTXTData(std::string p_path, std::string* p_data);
    bool loadFileData(std::string p_path, FILE* p_fp, char* p_data, int p_fileSize);
    bool saveFileData(std::string p_path, std::string p_data);
    bool appendDataToFile(std::string p_path, std::string p_data);
    std::ofstream* openFileToSave(std::string p_path);
    FILE* openFileToDownload(std::string p_path);
    void closeFile(std::ofstream* p_id);
    static size_t writeCurlDataCallBack(char* ptr, size_t size, size_t nmemb, void* userdata);
    static size_t readCurlDataCallBack(char* buffer, size_t size, size_t nitems, void* instream);
};
