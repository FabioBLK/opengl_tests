#include "FCamera.h"

FCamera::FCamera() {

}

void FCamera::render(Shader& p_shader) {
	glm::mat4 view = calculateView();
	glm::mat4 projection = calculateProjection();

	p_shader.use();
	p_shader.setMat4Float("view", view);
	p_shader.setMat4Float("projection", projection);
	p_shader.setVec3Float("viewPos", m_cameraPos.x, m_cameraPos.y, m_cameraPos.z);
}

void FCamera::moveForward(float p_deltaTime) {
	//glm::vec3 front = glm::vec3(m_cameraFront.x, 0, m_cameraFront.z); -> FPS Camera
	m_cameraPos += m_cameraFront * (m_speed * p_deltaTime);
}

void FCamera::moveBackwards(float p_deltaTime) {
	//glm::vec3 back = glm::vec3(m_cameraFront.x, 0, m_cameraFront.z); -> FPS Camera
	m_cameraPos -= m_cameraFront * (m_speed * p_deltaTime);
}

void FCamera::moveLeft(float p_deltaTime) {
	m_cameraPos -= glm::normalize(glm::cross(m_cameraFront, m_cameraUp)) * (m_speed * p_deltaTime);
}

void FCamera::moveRight(float p_deltaTime) {
	m_cameraPos += glm::normalize(glm::cross(m_cameraFront, m_cameraUp)) * (m_speed * p_deltaTime);
}

void FCamera::moveXAxis(float p_axixMove) {
	m_cameraPos += glm::normalize(glm::cross(m_cameraFront, m_cameraUp)) * m_speed * p_axixMove;
}

void FCamera::moveZAxis(float p_axixMove) {
	m_cameraPos -= m_cameraFront * m_speed * p_axixMove;
}

void FCamera::moveCameraView(float xoffset, float yoffset, float p_deltaTime) {
	const float sensitivity = 0.2f;
	xoffset *= sensitivity * p_deltaTime * m_rotSpeed;
	yoffset *= sensitivity * p_deltaTime * m_rotSpeed;

	m_yaw += xoffset;
	m_pitch -= yoffset;

	if (m_pitch > 89.0f) {
		m_pitch = 89.0f;
	}
	if (m_pitch < -89.0f) {
		m_pitch = -89.0f;
	}
}

glm::vec3 FCamera::getDirection() {
	glm::vec3 direction;
	direction.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
	direction.y = sin(glm::radians(m_pitch));
	direction.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));

	return direction;
}

glm::vec3 FCamera::getCameraFront() {
	m_cameraFront = glm::normalize(getDirection());
	return m_cameraFront;
}

glm::vec3 FCamera::getCameraPosition() {
	return m_cameraPos;
}

glm::vec3 FCamera::getCameraUp() {
	return m_cameraUp;
}

glm::vec3 FCamera::getCameraRight() {
	return glm::normalize(glm::cross(m_cameraFront, m_cameraUp));
}

float FCamera::getFov() {
	return m_fov;
}

glm::mat4 FCamera::calculateView() {
	glm::mat4 view = glm::mat4(1.0f);
	// note that we're translating the scene in the reverse direction of where we want to move
	//view = glm::translate(view, glm::vec3(0.0f, 0.0f, -5.0f));
	view = glm::lookAt(m_cameraPos, m_cameraPos + getCameraFront(), m_cameraUp);

	return view;
}

glm::mat4 FCamera::calculateProjection() {
	glm::mat4 projection = glm::mat4(1.0f);
	projection = glm::perspective(glm::radians(m_fov), (1280 * 1.0f) / (720 * 1.0f), 0.1f, 1000.0f);

	return projection;
}

void FCamera::setMovement(bool p_move) {
	m_move = p_move;
	if (m_move) {
		m_firstMove = true;
	}
}

void FCamera::MousePosition(double xpos, double ypos) {
	if (!m_move) {
		return;
	}

	if (m_firstMove) {
		m_lastX = xpos;
		m_lastY = ypos;

		m_firstMove = false;
	}

	float xoffset = xpos - m_lastX;
	float yoffset = ypos - m_lastY;
	m_lastX = xpos;
	m_lastY = ypos;

	const float sensitivity = 0.1f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	m_yaw += xoffset;
	m_pitch -= yoffset;

	if (m_pitch > 89.0f) {
		m_pitch = 89.0f;
	}
	if (m_pitch < -89.0f) {
		m_pitch = -89.0f;
	}
}

void FCamera::MouseScroll(double xoffset, double yoffset) {
	m_fov -= (float)yoffset;
	if (m_fov < 1.0f) {
		m_fov = 1.0f;
	}
	if (m_fov > 65.0f) {
		m_fov = 65.0f;
	}
}