#version 330 core

out vec4 FragColor;

uniform vec3 myColor;
uniform vec3 myLight;

void main()
{
   FragColor = vec4(myColor * myLight, 1.0f);
};