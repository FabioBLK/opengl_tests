#version 330 core

struct Material {
	vec3 diffuseColor;
	vec3 specularColor;
	sampler2D diffuse;
	sampler2D specular;
	float shineness;
};

struct DirLight {
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight {
	vec3 position;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct SpotLight {
	vec3 position;
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float cutoff;
	float outerCutOff;
};

out vec4 FragColor;

// Vertex shader input
in vec3 Normal;
in vec3 FragPos;
in vec2 TexCoord;


// Uniforms
uniform vec3 viewPos;
uniform Material material;
uniform DirLight dirLight;
uniform SpotLight spotLight;
uniform samplerCube skybox;

#define NR_POINT_LIGHTS 1
uniform PointLight pointLights[NR_POINT_LIGHTS];

// Functions Prototypes
vec4 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 reflection(vec3 fragPos, vec3 viewPos, vec3 normal);

void main()
{
	// properties
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(viewPos - FragPos);
	
	// Reflection map
	//vec3 reflection = reflection(FragPos, viewPos, norm);
	//vec4 reflectColor = texture(skybox, reflection);// + texture(material.specular, TexCoord);

	vec4 result = vec4(0.0);
	// phase 1: Directional light
	result = CalcDirLight(dirLight, norm, viewDir);
	//result += material.diffuseColor;

	// phase 2: Point lights
	for (int i = 0; i < NR_POINT_LIGHTS; i++) {
		result += vec4(CalcPointLight(pointLights[i], norm, FragPos, viewDir), 1.0);
	}
	
	// phase 3: Spot lights
	//result += CalcSpotLight(spotLight, norm, FragPos, viewDir);

	FragColor = vec4(result);
};

vec4 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir) 
{
	vec3 lightDir = normalize(-light.direction);

	// diffuse shading
	float diff = max(dot(normal, lightDir), 0.0);

	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shineness);

	vec4 diffuseColor = texture(material.diffuse, TexCoord);
	float alphaColor = diffuseColor.a;

	// combine results
	vec3 ambient = light.ambient * (vec3(texture(material.diffuse, TexCoord)) * material.diffuseColor);
	vec4 diffuse = vec4(light.diffuse, alphaColor) * diff * (vec4(texture(material.diffuse, TexCoord)) * vec4(material.diffuseColor, alphaColor));
	vec3 specular = light.specular * spec * (vec3(texture(material.specular, TexCoord)) * material.specularColor);

	return (vec4(ambient,alphaColor) + diffuse + vec4(specular,alphaColor));
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);
	
	// diffuse shading
	float diff = max(dot(normal, lightDir), 0.0);

	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shineness);

	// attenuation
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));

	// combine results
	vec3 ambient = light.ambient * (vec3(texture(material.diffuse, TexCoord)) * material.diffuseColor);
	vec3 diffuse = light.diffuse * diff * (vec3(texture(material.diffuse, TexCoord)) * material.diffuseColor);
	vec3 specular = light.specular * spec * (vec3(texture(material.specular, TexCoord)) * material.specularColor);

	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	return (ambient + diffuse + specular);
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir) 
{
	vec3 lightDir = normalize(light.position - fragPos);
	// light spot
	float tetha = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutoff - light.outerCutOff;
	float intensity = clamp((tetha - light.outerCutOff) / epsilon, 0.0, 1.0);

	// diffuse shading
	float diff = max(dot(normal, lightDir), 0.0);

	// specular shading
	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shineness);
	
	// combine results
	vec3 ambient = light.ambient * vec3(texture(material.diffuse, TexCoord));
	vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoord));
	vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoord));

	diffuse *= intensity;
	specular *= intensity;

	return (ambient + diffuse + specular);

	return vec3(1.0);
}

vec3 reflection(vec3 fragPos, vec3 viewPos, vec3 normal) {
	vec3 I = normalize(fragPos - viewPos);
	vec3 R = reflect(I, normal);

	return R;
}