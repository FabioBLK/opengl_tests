#pragma once
#include "FInput.h"
#include "Colors.h"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include <glm/glm.hpp>
#include <functional>

namespace FGui
{
	void Init(GLFWwindow* p_window);
	void FrameStart(FInput* p_input);
	void Render();
	void Terminate();
	void SceneWindow(float* p_cubeSize, FColor* p_dirLightColor, glm::vec3* p_dirLightPos, FColor* p_pointLightColor, 
		int* p_selectedItem, std::function<void()> callback);
	void SettingsWindow();
};

