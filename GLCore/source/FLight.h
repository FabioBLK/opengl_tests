#pragma once
#include "Shader.h"
#include "Colors.h"

enum class FLightType
{
	DIRECTIONAL,
	POINT,
	SPOT
};

class FLight
{
public:
	FLight(FLightType p_type);
	void Calculate(Shader* p_shader, FColor* p_color, glm::vec3* p_position);

private:
	void DirectionalLight(Shader* p_shader, FColor* p_color, glm::vec3* p_position);
	void PointLight(Shader* p_shader, FColor* p_color, glm::vec3* p_position, int p_lightIndex);
	void SpotLight(Shader* p_shader, FColor* p_color, glm::vec3* p_position);
	FLightType m_type;
};

