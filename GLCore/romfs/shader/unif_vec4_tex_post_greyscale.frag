#version 330 core

out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D ourTexture;

void main()
{
	vec4 texColor = texture(ourTexture, TexCoord);
	//float average = (texColor.r + texColor.g + texColor.b) / 3.0; // Version 1
	float average = (0.2126 * texColor.r + 0.7152 * texColor.g + 0.0722 * texColor.b); // Version 2
    FragColor = vec4(average, average, average, texColor.a);
};