#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;

out vec2 TexCoord;
out vec3 ourColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 myColor;
uniform vec3 myLight;
uniform vec3 lightPos;
uniform vec3 viewPos;

vec3 FragPos;
vec3 Normal;

void main()
{
	float ambientStrength = 0.1;
	vec3 ambient = ambientStrength * myLight;

	FragPos = vec3(model * vec4(aPos, 1.0));
	Normal = mat3(transpose(inverse(model))) * aNormal;
	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(lightPos - FragPos);

	float diff = max(dot(norm, lightDir), 0.0);
	vec3 difuse = diff * myLight;

	float specularStrength = 0.75;
	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);

	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 specular = specularStrength * spec * myLight;

	vec3 result = (ambient + difuse + specular) * myColor;

	gl_Position = projection * view *  model * vec4(aPos.x, aPos.y, aPos.z, 1.0);
	
	TexCoord = aTexCoord;
	ourColor = result;
};