#pragma once

struct FColor
{
	float r;
	float g;
	float b;
	float a;
};

namespace Colors {
	FColor Yellow();
	FColor White();
	FColor Red();
	FColor Green();
	FColor Blue();
};