#pragma once
#include "IInput.h"
#include "Shader.h"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class FCamera : public IInput
{
public:
	FCamera();
	void render(Shader& p_shader);
	void moveForward(float p_deltaTime);
	void moveBackwards(float p_deltaTime);
	void moveLeft(float p_deltaTime);
	void moveRight(float p_deltaTime);
	void moveXAxis(float p_axixMove);
	void moveZAxis(float p_axixMove);
	void moveCameraView(float xoffset, float yoffset, float p_deltaTime);
	glm::vec3 getDirection();
	glm::vec3 getCameraFront();
	glm::vec3 getCameraPosition();
	glm::vec3 getCameraUp();
	glm::vec3 getCameraRight();
	float getFov();
	glm::mat4 calculateView();
	glm::mat4 calculateProjection();
	void setMovement(bool p_move);
	virtual void MousePosition(double xpos, double ypos);
	virtual void MouseScroll(double xoffset, double yoffset);

private:
	float m_lastX = 1280 / 2;
	float m_lastY = 720 / 2;
	float m_yaw = 0.0f;
	float m_pitch = 0.0f;
	float m_fov = 45.0f;
	glm::vec3 m_cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
	glm::vec3 m_cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 m_cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);
	float m_speed = 2.5f;
	float m_rotSpeed = 200.0f;
	bool m_move = true;
	bool m_firstMove = true;
};

