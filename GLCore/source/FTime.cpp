#include "FTime.h"

FTime::FTime() {
	m_currentFrame = 0.0f;
	m_lastFrame = 0.0f;
	m_deltaTime = 0.0f;
}

float FTime::deltaTime() {
	m_currentFrame = glfwGetTime();
	m_deltaTime = m_currentFrame - m_lastFrame;
	m_lastFrame = m_currentFrame;

	return m_deltaTime;
}

float FTime::getTime() {
	return glfwGetTime();
}
