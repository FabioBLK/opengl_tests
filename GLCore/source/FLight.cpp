#include "FLight.h"

FLight::FLight(FLightType p_type) {
	m_type = p_type;
}

void FLight::Calculate(Shader* p_shader, FColor* p_color, glm::vec3* p_position) {
	if (m_type == FLightType::DIRECTIONAL) {
		DirectionalLight(p_shader, p_color, p_position);
	}
	else if (m_type == FLightType::POINT) {
		PointLight(p_shader, p_color, p_position, 0);
	}
	else {
		SpotLight(p_shader, p_color, p_position);
	}
}

void FLight::DirectionalLight(Shader* p_shader, FColor* p_color, glm::vec3* p_position) {
	p_shader->use();
	p_shader->setVec3Float("dirLight.direction", p_position->x, p_position->y, p_position->z);
	p_shader->setVec3Float("dirLight.ambient", p_color->r, p_color->g, p_color->b);
	p_shader->setVec3Float("dirLight.diffuse", p_color->r, p_color->g, p_color->b);
	p_shader->setVec3Float("dirLight.specular", p_color->r, p_color->g, p_color->b);
}

void FLight::PointLight(Shader* p_shader, FColor* p_color, glm::vec3* p_position, int p_lightIndex) {
	p_shader->use();
	p_shader->setVec3Float("pointLights[" + std::to_string(p_lightIndex) + "].position", p_position->x, p_position->y, p_position->z);
	p_shader->setVec3Float("pointLights[" + std::to_string(p_lightIndex) + "].ambient", p_color->r * 0.2f, p_color->g * 0.2f, p_color->b * 0.2f);
	p_shader->setVec3Float("pointLights[" + std::to_string(p_lightIndex) + "].diffuse", p_color->r, p_color->g, p_color->b);
	p_shader->setVec3Float("pointLights[" + std::to_string(p_lightIndex) + "].specular", p_color->r, p_color->g, p_color->b);
	p_shader->setFloat("pointLights[" + std::to_string(p_lightIndex) + "].constant", 1.0f);
	p_shader->setFloat("pointLights[" + std::to_string(p_lightIndex) + "].linear", 0.09f);
	p_shader->setFloat("pointLights[" + std::to_string(p_lightIndex) + "].quadratic", 0.032f);
}

void FLight::SpotLight(Shader* p_shader, FColor* p_color, glm::vec3* p_position) {

}