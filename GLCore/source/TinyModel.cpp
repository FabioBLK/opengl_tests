#if defined _MSC_VER
#include "TinyModel.h"
#define TINYOBJLOADER_IMPLEMENTATION
#else
#define TINYOBJLOADER_IMPLEMENTATION
#include "TinyModel.h"
#endif

TinyModel::TinyModel(std::string path, bool p_flipTexure) {
	m_modelMatrix = glm::mat4(1.0f);
	m_position = glm::vec3(0.0f);
	m_scale = glm::vec3(1.0f);
	m_flipTexture = p_flipTexure;
	m_whiteTextureId = WhiteTexture();
	loadModel(path);
}

void TinyModel::DrawModel(Shader& shader) {
	shader.use();

	m_modelMatrix = glm::mat4(1.0f);
	m_modelMatrix = glm::translate(m_modelMatrix, m_position);
	m_modelMatrix = glm::scale(m_modelMatrix, m_scale);

	shader.setMat4Float("model", m_modelMatrix);

	for (unsigned int i = 0; i < m_meshes.size(); i++) {
		m_meshes[i].Draw(shader, m_materials_loaded[m_meshes[i].m_materialIndex], m_whiteTextureId, false, 0);
	}
}

void TinyModel::Draw(Shader& shader) {
	shader.use();
	for (unsigned int i = 0; i < m_meshes.size(); i++) {
		m_meshes[i].Draw(shader, m_materials_loaded[m_meshes[i].m_materialIndex], m_whiteTextureId, false, 0);
	}
}

void TinyModel::DrawInstanced(Shader& shader, const unsigned int p_amount) {
	for (unsigned int i = 0; i < m_meshes.size(); i++) {
		m_meshes[i].Draw(shader, m_materials_loaded[m_meshes[i].m_materialIndex], m_whiteTextureId, true, p_amount);
	}
}

void TinyModel::SetInstances(const unsigned int p_amount, glm::mat4* p_matrices) {
	unsigned int buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, p_amount * sizeof(glm::mat4), &p_matrices[0], GL_STATIC_DRAW);

	// set transformation matrices as an instance vertex attribute (with divisor 1)
	// note: we're cheating a little by taking the, now publicly declared, VAO of the model's mesh(es) and adding new vertexAttribPointers
	// normally you'd want to do this in a more organized fashion, but for learning purposes this will do.
	for (unsigned int i = 0; i < m_meshes.size(); i++) {
		unsigned int VAO = m_meshes[i].GetVAO();
		glBindVertexArray(VAO);
		// set attribute pointers for matrix (4 times vec4)
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);

		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));

		glEnableVertexAttribArray(5);
		glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));

		glEnableVertexAttribArray(6);
		glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));

		glVertexAttribDivisor(3, 1);
		glVertexAttribDivisor(4, 1);
		glVertexAttribDivisor(5, 1);
		glVertexAttribDivisor(6, 1);

		glBindVertexArray(0);
	}
}

void TinyModel::SetPosition(const glm::vec3* p_position) {
	m_position = *p_position;
}

void TinyModel::SetScale(const glm::vec3* p_scale) {
	m_scale = *p_scale;
}

void TinyModel::loadModel(std::string path) {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warning;
	std::string error;

	std::size_t found = path.find_last_of("/\\");
	m_directory = path.substr(0, found);

	bool status = tinyobj::LoadObj(&attrib, &shapes, &materials, &warning, &error, path.c_str(), m_directory.c_str());

	if (!warning.empty()) {
		std::string message = "TINYMODEL::Warning loading model " + warning;
		FLogs::shared_instance().Log(message, FLogType::FWARNING);
	}

	if (!error.empty()) {
		std::string message = "TINYMODEL::Error loading model " + error;
		FLogs::shared_instance().Log(message, FLogType::FERROR);
	}

	if (!status) {
		std::string message = "TINYMODEL::Error loading file " + path;
		FLogs::shared_instance().Log(message, FLogType::FERROR);
		return;
	}
	else {
		std::string message = "TINYMODEL::Success loading file " + path;
		FLogs::shared_instance().Log(message, FLogType::FDEBUG);

		processModel(&attrib, shapes, materials);
	}
}

void TinyModel::processModel(tinyobj::attrib_t* p_attrib, std::vector<tinyobj::shape_t>& p_shapes, std::vector<tinyobj::material_t>& p_materials) {
	for (unsigned int i = 0; i < p_shapes.size(); i++) {
		Mesh mesh = processMesh(p_attrib, &p_shapes[i].mesh, p_materials);
		m_meshes.push_back(mesh);
	}
}

Mesh TinyModel::processMesh(tinyobj::attrib_t* p_attrib, tinyobj::mesh_t* p_mesh, std::vector<tinyobj::material_t>& p_materials) {
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;

	for (unsigned int i = 0; i < p_mesh->indices.size(); i++) {
		Vertex vertex;

		// Get Position
		glm::vec3 position;
		int indexX = 3 * p_mesh->indices[i].vertex_index + 0;
		int indexY = 3 * p_mesh->indices[i].vertex_index + 1;
		int indexZ = 3 * p_mesh->indices[i].vertex_index + 2;
		position.x = p_attrib->vertices[indexX];
		position.y = p_attrib->vertices[indexY];
		position.z = p_attrib->vertices[indexZ];

		vertex.Position = position;

		// Get Normal
		glm::vec3 normal;
		int nIndexX = 3 * p_mesh->indices[i].normal_index + 0;
		int nIndexY = 3 * p_mesh->indices[i].normal_index + 1;
		int nIndexZ = 3 * p_mesh->indices[i].normal_index + 2;
		normal.x = p_attrib->normals[nIndexX];
		normal.y = p_attrib->normals[nIndexY];
		normal.z = p_attrib->normals[nIndexZ];

		vertex.Normal = normal;

		// Get TexCoord
		glm::vec2 texCoord;
		int tIndexX = 2 * p_mesh->indices[i].texcoord_index + 0;
		int tIndexY = 2 * p_mesh->indices[i].texcoord_index + 1;
		texCoord.x = p_attrib->texcoords[tIndexX];
		texCoord.y = p_attrib->texcoords[tIndexY];

		vertex.TexCoords = texCoord;

		indices.push_back(i);
		vertices.push_back(vertex);
	}

	int materialIndex = -1;
	if (p_mesh->material_ids.size() > 0) {
		if (p_mesh->material_ids[0] >= 0) {
			FLogs::shared_instance().Log("Processing Material", FLogType::FDEBUG);
			Material mat;

			materialIndex = p_mesh->material_ids[0];
			tinyobj::material_t tinyMaterial = p_materials[materialIndex];

			glm::vec3 dColor = glm::vec3(1.0f, 1.0f, 1.0f);
			if (sizeof(tinyMaterial.diffuse) / sizeof(*tinyMaterial.diffuse) >= 3) {
				dColor.x = tinyMaterial.diffuse[0];
				dColor.y = tinyMaterial.diffuse[1];
				dColor.z = tinyMaterial.diffuse[2];
			}

			glm::vec3 sColor = glm::vec3(1.0f, 1.0f, 1.0f);
			if (sizeof(tinyMaterial.specular) / sizeof(*tinyMaterial.specular) >= 3) {
				sColor.x = tinyMaterial.specular[0];
				sColor.y = tinyMaterial.specular[1];
				sColor.z = tinyMaterial.specular[2];
			}

			std::vector<Texture> textures;
			if (!tinyMaterial.diffuse_texname.empty()) {
				Texture dTexture = loadMaterialTexture(&tinyMaterial, MeshTextureType::TEXTURE_DIFFUSE);
				textures.push_back(dTexture);
			}
			if (!tinyMaterial.specular_texname.empty()) {
				Texture sTexture = loadMaterialTexture(&tinyMaterial, MeshTextureType::TEXTURE_DIFFUSE);
				textures.push_back(sTexture);
			}

			mat.id = materialIndex;
			mat.diffuseColor = dColor;
			mat.specularColor = sColor;
			mat.shineness = tinyMaterial.shininess;
			mat.textures = textures;

			if (m_materials_loaded.find(materialIndex) == m_materials_loaded.end()) {
				m_materials_loaded.emplace(materialIndex, mat);
			}
		}
	}

	if (materialIndex < 0) {
		materialIndex = m_materials_loaded.size();
		FLogs::shared_instance().Log("Processing Material FALLBACK", FLogType::FDEBUG);
		Material mat;
		mat.id = 0;
		mat.diffuseColor = glm::vec3(1.0f);
		mat.specularColor = glm::vec3(1.0f);
		mat.shineness = 1.0f;

		if (m_materials_loaded.find(materialIndex) == m_materials_loaded.end()) {
			m_materials_loaded.emplace(materialIndex, mat);
		}
	}

	return Mesh(vertices, indices, materialIndex);
}

Texture TinyModel::loadMaterialTexture(tinyobj::material_t* p_mat, MeshTextureType meshType) {
	Texture texture;
	std::string str;

	if (meshType == MeshTextureType::TEXTURE_SPECULAR) {
		str = p_mat->specular_texname;
	}
	else {
		str = p_mat->diffuse_texname;
	}

	bool skip = false;
	for (unsigned int j = 0; j < textures_loaded.size(); j++) {
		if (std::strcmp(str.c_str(), textures_loaded[j].path.data()) == 0) {
			texture = textures_loaded[j];
			skip = true;
			break;
		}
	}

	if (!skip) {
		texture.id = TextureFromFile(str, m_directory);
		texture.type = meshType;
		texture.path = str;

		textures_loaded.push_back(texture);
	}

	return texture;
}

unsigned int TinyModel::TextureFromFile(std::string path, std::string directory) {
	std::string filePath = directory + "/" + path;
	std::string loadingTexureMessage = "loading texture at " + filePath;
	FLogs::shared_instance().Log(loadingTexureMessage);
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load and generate the texture
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(!m_flipTexture);
	unsigned char* data = stbi_load(filePath.c_str(), &width, &height, &nrChannels, 0);
	if (data) {
		GLenum format;
		if (nrChannels == 4) {
			format = GL_RGBA;
		}
		else {
			format = GL_RGB;
		}
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Required on Alpha Romeo Textures
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		std::string successMessage = "INFO::MODEL::SUCCESS to load texture " + filePath;
		FLogs::shared_instance().Log(successMessage);
	}
	else {
		FLogs::shared_instance().Log("ERROR::MODEL::Failed to load texture");
	}
	stbi_image_free(data);

	return texture;
}

unsigned int TinyModel::WhiteTexture() {
	FLogs::shared_instance().Log("INFO::MODEL::Generating white texture");
	unsigned int texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// load and generate the texture
	int width = 1;
	int height = 1;

	unsigned char* data = new unsigned char[width * height * sizeof(unsigned char)];
	for (int i = 0; i < (int)(width * height * sizeof(unsigned char)); i++)
	{
		data[i] = 255;
	}

	if (data) {
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Required on Alpha Romeo Textures
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		FLogs::shared_instance().Log("INFO::MODEL::SUCCESS to load texture");
	}
	else {
		FLogs::shared_instance().Log("ERROR::MODEL::Failed to load texture");
	}

	return texture;
}