#pragma once
#include "FLogs.h"

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

class Shader
{
public:
	// the program ID
	unsigned int ID;
	// constructor reads and builds the shader
	Shader(const char* vertexPath, const char* fragmentPath);
	Shader(const char* vertexPath, const char* fragmentPath, const char* geometryPath);

	std::string getShaderCode(const char* path);
	unsigned int compileVertexShader(const char* shaderCode);
	unsigned int compileFragmentShader(const char* shaderCode);
	unsigned int compileGeometryShader(const char* shaderCode);
	unsigned int linkShader(std::vector<unsigned int> shaders);
	// use/activate the shader
	void use();
	// utility uniform functions
	void setBool(const std::string& name, bool value) const;
	void setInt(const std::string& name, int value) const;
	void setFloat(const std::string& name, float value) const;
	void setVec4Float(const std::string& name, float valueR, float valueG, float valueB, float valueA) const;
	void setMat4Float(const std::string& name, const glm::mat4 matrix) const;
	void setVec3Float(const std::string& name, float valueR, float valueG, float valueB) const;
	void setUniformBlockInt(const std::string& name, const int value) const;
};

