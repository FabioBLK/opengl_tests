#include "FLogs.h"

FLogs::FLogs() {
	m_logToFile = false;
}

FLogs::FLogs(bool p_logToFile) {
	m_logToFile = p_logToFile;
	m_files = Files();
	std::string configDir = "logs";
	bool exists = m_files.isDirValid(configDir);
	if (!exists)
	{
		bool dirCreated = m_files.createDir(configDir);
	}

	m_filePath = "logs/log.txt";
	if (m_logToFile) {
		m_files.appendDataToFile(m_filePath, "Startint LOG\n");
	}
}

void FLogs::Log(std::string p_message, FLogType p_type) {
	std::chrono::system_clock::time_point nowTime = std::chrono::system_clock::now();
	std::time_t currentTime = std::chrono::system_clock::to_time_t(nowTime);
	std::string timeStr = std::ctime(&currentTime);
	std::size_t found = timeStr.find_last_of("\n");
	timeStr = timeStr.substr(0, found);

	std::string message = timeStr + "::" + m_typeStr[p_type] + p_message + "\n";

	std::cout << message;

	if (m_logToFile) {
		LogToFile(message);
	}
}

void FLogs::LogToFile(std::string p_message) {
	m_files.appendDataToFile(m_filePath, p_message);
}